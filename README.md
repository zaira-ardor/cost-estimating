# **home-depot-product-extractor**

> Extract schema.org/[Product ![External resource][octicon-link-external]][schema-org-product] information from HomeDepot.com.

<!-- Badges -->

## Table of contents

<!-- toc -->

+ [1. **Installation**](#1-installation)
+ [2. **Usage**](#2-usage)
  + [2.1. Crawl & extract `schema.org/Products`](#21-crawl--extract-schemaorgproducts)
  + [2.2. Export extracted `schema.org/Products`](#22-export-extracted-schemaorgproducts)
+ [3. **Support**](#3-support)
+ [4. **Roadmap**](#4-roadmap)
+ [5. **Architectural decision records**](#5-architectural-decision-records)
+ [6. **Contributing**](#6-contributing)
+ [7. **Licenses**](#7-licenses)

<!-- tocstop -->

## 1. **Installation**

```bash
npm install home-depot-product-extractor
```

## 2. **Usage**

### 2.1. Crawl & extract `schema.org/Products`

![File source code][octicon-file-code]

```typescript
import HomeDepotProductExtractor, {
  homeDepotCrawlerOptions
} from './crawlers/hd'

homeDepotCrawlerOptions.maxRequestsPerCrawl = 20
const allProductSitemapUrls = [
  'https://www.homedepot.com/sitemap/P/PIPs.xml'
]
const extractor = new HomeDepotProductExtractor(homeDepotCrawlerOptions)
await extractor.run(allProductSitemapUrls)
```

![Terminal][octicon-terminal] Once done, verify that you've stored the products in a Terminal:

```bash
# ⚠︎ Linux and macOS only
ls ./storage/datasets/default/*.json | wc -1
# => 955146 (or thereabout 😇)
```

### 2.2. Export extracted `schema.org/Products`

![Terminal][octicon-terminal] When crawling and extraction have finished, you can export the results
to a deflated `.zip` file via a Terminal:

```bash
# ⚠︎ Linux and macOS only
function toSafeIsoDate () {
  isoDate=$(date -u +"%Y-%m-%dT%H-%M-%SZ")
  echo "$isoDate"
}

function nameExportZipFile() {
  name=$1
  infix=$(toSafeIsoDate)
  ext=$2
  echo "${name}.${infix}.${ext}"
}

defaultArchiveName=$(nameExportZipFile "hd-products" "zip")
productStorageDir="./storage/datasets/default"

zip -r "${defaultArchiveName}" . -i "${productStorageDir}"
# => creates ./hd-products.2024-03-16T21-28-14Z.zip
```

## 3. **Support**

If you need help, please [create an issue](issues).

## 4. **Roadmap**

The _home-depot-product-extractor_ is the first of several ELT/ETL modules.

<dl>
  <dt>Extractors</dt>
  <dd>

`Extractors` crawl websites looking for [schema.org/`Product`](https://schema.org/Product) data.
If found, these data are stored temporarily.

```javascript
product.data.extractors = ['home-depot-product-extractor']
```

  </dd>
  <dt>Loaders</dt>
  <dd>

`Loaders` save the data from `Extractors` into a database.

```javascript
product.data.loaders = ['mongo-d-b-loader', 'rethink-d-b-loader']
```

  </dd>
  <dt>Transformers</dt>
  <dd>

`Transformers` reshape the loaded [schema.org/`Product`s](https://schema.org/Product).

```javascript
product.data.transformers.enrichers = ['product-offer-enricher']
product.data.transformers.replacers = []
product.data.transformers.revivers = []
```

  </dd>
</dl>

## 5. **Architectural decision records**

> We document every architecture-level decision for @zaira-ardor-llc and its core modules with an
> Architectural Decision Record <abbr title="Architectural Decision Record">ADR</abbr>.

View this product's [architectural decision records](docs/adr/).

For more information about <abbr title="Architectural Decision Record">ADR</abbr>s, please see
[0001. Record architecture decisions![external link][octicon-link-external]][adr-zallc-0001] for details.

## 6. **Contributing**

Please read our [Code of conduct](CODE_OF_CONDUCT.md) and [Contributing](CONTRIBUTING.md) guidelines.

## 7. **Licenses**

[Apache License 2.0](LICENSE) © Zaira Ardor LLC

[![FOSSA Status][fossa-status-badge]][fossa-license-report]

<!-- Resource references -->

[adr-zallc-0001]: https://gitlab.com/zaira-ardor-llc/architecture/adr/-/blob/main/docs/0001-record-architecture-decisions.md#0001-record-architecture-decisions
[fossa-status-badge]: https://app.fossa.com/api/projects/custom%2B43688%2Fgitlab.com%2Fzaira-ardor-llc%2Fproducts%2Fhome-depot-product-extractor.svg?type=large&issueType=license
[fossa-license-report]: https://app.fossa.com/projects/custom%2B43688%2Fgitlab.com%2Fzaira-ardor-llc%2Fproducts%2Fhome-depot-product-extractor?ref=badge_large&issueType=license

[octicon-file-code]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/file-code.svg
[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/link-external.svg
[octicon-terminal]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/terminal.svg
[schema-org-product]: https://schema.org/Product
