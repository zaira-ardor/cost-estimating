import impactWrenchMock from './impact-wrench.mock.json'

const productPageMock = `<!doctype html>
<html lang="en">
  <head>
    <script data-th="server" type="application/ld+json" id="thd-helmet__script--productStructureData">${JSON.stringify(
      impactWrenchMock
    )}</script>
  </head>
  <body>
    <h1>18V Brushless Cordless 4-Mode 1/2 in. High-Torque Impact Wrench Kit with (2) 4.0 Ah Lithium-Ion Batteries and Charger</h1>
  </body>
</html>`

export { productPageMock }
