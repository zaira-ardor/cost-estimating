const mainSitemapXml = `
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <sitemap>
    <!-- Paid home services pages -->
    <loc>https://www.homedepot.com/sitemap/HomeServices.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Product Information Pages -->
    <loc>https://www.homedepot.com/sitemap/P/PIPs.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Product List Pages -->
    <loc>https://www.homedepot.com/sitemap/B/PLPs.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Natural languages served -->
    <loc>https://www.homedepot.com/sitemap/Hreflang/hreflang.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Store locations -->
    <loc>https://www.homedepot.com/sitemap/PPS.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Category pages -->
    <loc>https://www.homedepot.com/sitemap/Cat.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Collections (e.g., products arranged by purpose or room) -->
    <loc>https://www.homedepot.com/sitemap/Collections.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Brand pages -->
    <loc>https://www.homedepot.com/sitemap/B/Brands.xml</loc>
  </sitemap>
  <sitemap>
    <!-- Non-product pages -->
    <loc>https://www.homedepot.com/sitemap/C/Content.xml</loc>
  </sitemap>
  <sitemap>
    <!-- How-to & DIY (Do It Yourself) articles -->
    <loc>https://www.homedepot.com/sitemap/C/Content/Article.xml</loc>
  </sitemap>
</sitemapindex>
`

export default mainSitemapXml
