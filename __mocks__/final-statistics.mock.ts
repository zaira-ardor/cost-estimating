import { type FinalStatistics } from 'crawlee'

const finalStatisticsMock: FinalStatistics = {
    crawlerRuntimeMillis: 0,
    requestAvgFailedDurationMillis: 0,
    requestAvgFinishedDurationMillis: 0,
    requestsFailed: 0,
    requestsFailedPerMinute: 0,
    requestsFinished: 0,
    requestsFinishedPerMinute: 0,
    requestsTotal: 0,
    requestTotalDurationMillis: 0,
    retryHistogram: [0]
} satisfies FinalStatistics

export default finalStatisticsMock
