import { type CheerioCrawlingContext, parseWithCheerio } from '@crawlee/cheerio'
import { CheerioAPI } from 'cheerio'
import { type Dictionary } from '@crawlee/core'
import { type InternalHttpCrawlingContext } from '@crawlee/http'



const cheerioCrawlingContextMock: CheerioCrawlingContext = {
  $: parseWithCheerio,
  body: '',
  json: {},
  contentType: { type: 'application/json' },
  response: unknown,
  parseWithCheerio,
  addRequests(urls: string[]): Promise<void> {
    void urls
    return Promise.resolve(void)
  }
}

export default cheerioCrawlingContextMock
