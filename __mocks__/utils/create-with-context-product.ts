import { type Product, type WithContext } from 'schema-dts'

const createWithContextProduct = (shape: object): WithContext<Product> => {
  const json = JSON.stringify(shape)
  return JSON.parse(json) as WithContext<Product>
}

export default createWithContextProduct
