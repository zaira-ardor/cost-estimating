import nock from 'nock'
import mainSitemapXmlMock from '../../sitemaps/main-sitmap-xml.mock'
import robotsTxt from '../../robots/robots.txt'
import pPips210010XmlMock from '../../sitemaps/p-pips-21-001-0-xml.mock'
import pPips21XmlMock from '../../sitemaps/p-pips-21-xml.mock'
import pPipsXmlMock from '../../sitemaps/p-pips-xml.mock'
import {productPageMock} from '../../p/product-page.mock'

const mockUrlList = [
  'https://www.homedepot.com/b/Building-Materials-Siding/N-5yc1vZarip',
  'https://www.homedepot.com/p/1-2-4-ft-x-8-ft-Oriented-Strand-Board-660663/100072564',
  'https://www.homedepot.com/sitemap/B/PLPs.xml',
  'https://www.homedepot.com/sitemap/B/PLPs/21-LUMBER/21-LUMBER-0.xml',
  'https://www.homedepot.com/sitemap/P/PIPs.xml',
  'https://www.homedepot.com/sitemap/P/PIPs/21/021-001-0.xml'
]

const setUpHttpInterceptors = () => {
  nock.disableNetConnect()
  nock('https://www.homedepot.com')
    .persist()
    .get('/robots.txt')
    .reply(200, robotsTxt)
    .get('/robots.404.txt')
    .reply(404)
    .get('/sitemap/main.xml')
    .reply(200, mainSitemapXmlMock)
    .get('/sitemap/P/PIPs.xml')
    .reply(200, pPipsXmlMock)
    .get('/mock-resource')
    .reply(200)
    .get('/p/mock-product-page/123456789')
    .reply(200, productPageMock)
    .get('/mock-server-error')
    .reply(500, {
      code: 'ERR_MOCK_SERVER_ERROR',
      status: 500,
      statusCode: 500
    })
    .get('*')
    .reply(404)
}

const tearDownHttpInterceptors = () => {
  nock.cleanAll()
  nock.enableNetConnect()
}

export { setUpHttpInterceptors, tearDownHttpInterceptors }
