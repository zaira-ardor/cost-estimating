import { RequestOptions } from 'crawlee'

const requestOptions: RequestOptions = {
  id: 'id',
  url: '',
  loadedUrl: '',
  uniqueKey: 'uniqueKey',
  payload: 'payload',
  noRetry: false,
  retryCount: 0,
  sessionRotationCount: 0,
  maxRetries: 2,
  errorMessages: [],
  headers: {},
  userData: {},
  label: undefined,
  handledAt: new Date().toISOString(),
  keepUrlFragment: false,
  useExtendedUniqueKey: false,
  skipNavigation: true,
  enqueueStrategy: undefined
} as RequestOptions

const createRequestOptions = (url: string): RequestOptions => {
  return Object.assign({}, requestOptions, {
    url,
    loadedUrl: url
  }) as RequestOptions
}

export default requestOptions
export { createRequestOptions }
