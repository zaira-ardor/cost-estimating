import { describe, expect, test } from '@jest/globals'
import { constants } from '../../../src/crawlers'
describe('constants', () => {
  test('provide common, readonly values', () => {
    expect(constants).toBeDefined
  })
})
describe('constants.selectors', () => {
  describe('define CSS selectors that identify DOM elements:', () => {
    test('constants.selectors.product', () => {
      expect(constants.selectors.product).toBe(
        'script[id$=productStructureData]'
      )
    })
  })
})
describe('constants.urls.robots', () => {
  test('is the default URL for Home Depot\'s "robots.txt"', () => {
    expect(constants.urls.robots).toBe('https://www.homedepot.com/robots.txt')
  })
})
describe('constants.urls.sitemaps', () => {
  describe('declare important Home Depot sitemaps, such as', () => {
    test('constants.urls.sitemaps.products', () => {
      expect(constants.urls.sitemaps.products).toBeDefined()
    })
    test('constants.urls.sitemaps.main', () => {
      expect(constants.urls.sitemaps.main).toBeDefined()
    })
    test('constants.urls.sitemaps.brands', () => {
      expect(constants.urls.sitemaps.brands).toBeDefined()
    })
    test('constants.urls.sitemaps.categories', () => {
      expect(constants.urls.sitemaps.categories).toBeDefined()
    })
    test('constants.urls.sitemaps.homeServices', () => {
      expect(constants.urls.sitemaps.homeServices).toBeDefined()
    })
    test('constants.urls.sitemaps.productLists', () => {
      expect(constants.urls.sitemaps.productLists).toBeDefined()
    })
    test('constants.urls.sitemaps.stores', () => {
      expect(constants.urls.sitemaps.stores).toBeDefined()
    })
  })
})
