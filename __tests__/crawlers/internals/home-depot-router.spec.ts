import {
  afterAll,
  beforeAll,
  describe,
  expect,
  jest,
  test
} from '@jest/globals'
import { type CheerioCrawlingContext } from '@crawlee/cheerio'
import { Dataset, type Dictionary, Request, RequestOptions } from 'crawlee'
import { type Product, type WithContext } from 'schema-dts'
import HomeDepotProductExtractor, {homeDepotRouter} from '../../../src/crawlers'
import finalStatisticsMock from '../../../__mocks__/final-statistics.mock'
import { createRequestOptions } from '../../../__mocks__/utils/network/request-options.mock'
import { setUpHttpInterceptors, tearDownHttpInterceptors } from '../../../__mocks__/utils/network/nock-setup-teardown'

describe('homeDepotRouter', () => {
  beforeAll(() => {
    setUpHttpInterceptors()
  })

  afterAll(() => {
    tearDownHttpInterceptors()
  })

  test('stores schema.org/Products', () => {
    // expect.hasAssertions()
    // const url = 'https://www.homedepot.com/p/mock-product-page/123456789'
    // const pushDataSpy = jest.spyOn(Dataset, 'pushData')
    // const extractor = new HomeDepotProductExtractor(url)

    // await expect(extractor.run()).resolves
    // expect(pushDataSpy).toHaveBeenCalled()
  })
  test('enqueues sitemap locations', () => {

  })
})
