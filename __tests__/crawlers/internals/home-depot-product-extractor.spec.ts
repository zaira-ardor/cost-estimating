import {
  afterAll,
  beforeAll,
  describe,
  expect,
  jest,
  test
} from '@jest/globals'
import {
  Configuration,
  type FinalStatistics,
  Request,
  type RequestOptions } from 'crawlee'
import finalStatisticsMock from '../../../__mocks__/final-statistics.mock'
import requestOptionsMock from '../../../__mocks__/utils/network/request-options.mock'
import HomeDepotProductExtractor, {
  constants,
  defaultCrawlerRunOptions,
  HomeDepotCrawler,
  homeDepotCrawlerOptions,
  HomeDepotCrawlerOptions
} from '../../../src/crawlers'

import { setUpHttpInterceptors, tearDownHttpInterceptors } from '../../../__mocks__/utils/network/nock-setup-teardown'
import robotsTxt from '../../../__mocks__/robots/robots.txt'
import pPipsXmlMock from '../../../__mocks__/sitemaps/p-pips-xml.mock'

describe('class HomeDepotProductExtractor', () => {

  beforeAll(() => {
    setUpHttpInterceptors()
  })

  afterAll(() => {
    tearDownHttpInterceptors()
  })

  describe('follows the Proxy design pattern', () => {
    describe('in order to simplify how we', () => {
      test('initialize a crawl', () => {
        const extractor = new HomeDepotProductExtractor()
        expect(extractor).toBeDefined()
        expect(extractor!.url).toBe(constants.urls.sitemaps.products)
        expect(extractor!.options).toBeInstanceOf(HomeDepotCrawlerOptions)
        expect(extractor!.config).toBeInstanceOf(Configuration)
        expect(extractor!.crawler).toBeInstanceOf(HomeDepotCrawler)
        expect(extractor!.robots).toBeDefined()
        expect(extractor!.sitemap).toBeDefined()
      })

      describe('run crawls & Product extraction', () => {
        test('with an array of URLs', async () => {
          const extractor = new HomeDepotProductExtractor()
          const runSpy = jest.spyOn(extractor, 'run')
            .mockReturnValue(Promise.resolve(finalStatisticsMock))

            await expect(extractor!.run()).resolves
            expect(runSpy).toHaveBeenCalled()
          })

        test('with an array of Requests', async () => {
          const extractor = new HomeDepotProductExtractor()
          const requests = [new Request({
            url: 'https://www.homedepot.com/mock-resource'
          })]
          const runSpy = jest.spyOn(extractor.crawler, 'run')
            .mockReturnValue(Promise.resolve(finalStatisticsMock))

          await expect(extractor!.run(requests)).resolves
          expect(runSpy).toHaveBeenCalled()
        })

        test('with an array of RequestsOptions', async () => {
          const extractor = new HomeDepotProductExtractor()
          const requestOptionsList = [requestOptionsMock] as RequestOptions[]
          const runSpy = jest.spyOn(extractor.crawler, 'run')
            .mockReturnValue(Promise.resolve(finalStatisticsMock))

          await expect(extractor!.run(requestOptionsList)).resolves
          expect(runSpy).toHaveBeenCalled()
        })
      })
    })
  })
})
