import { describe, expect, test } from '@jest/globals'
import handleCatchBlock from '../../../src/crawlers/internals/handle-catch-block'

describe('handleCatchBlock(error: unknown) returns an Error', () => {

  describe(`with most "error" parameter values:`, () => {

    test('@parameter {Error} error', () => {
      const error = new Error()
      const actual = handleCatchBlock(error)
      expect(actual).toBeInstanceOf(Error)
      expect(actual).toBe(error)
    })

    test('@parameter {string} error', () => {
      const error = `handleCatchBlock accepts string args`
      const actual = handleCatchBlock(error)
      expect(actual).toBeInstanceOf(Error)
      expect(actual.message).toBe(`"${error}"`)
    })

    test('@parameter {boolean} error', () => {
      const error = false
      const actual = handleCatchBlock(error)
      expect(actual).toBeInstanceOf(Error)
      expect(actual.message).toBe('false')
    })

    test('@parameter {number} error', () => {
      const error = 666
      const actual = handleCatchBlock(error)
      expect(actual).toBeInstanceOf(Error)
      expect(actual.message).toBe('666')
    })

    test('@parameter {object} error', () => {
      const error = { a: '1', b: { b1: 'b1', b2: 'b2' } }
      const actual = handleCatchBlock(error)
      expect(actual).toBeInstanceOf(Error)
      expect(actual.message).toBe(JSON.stringify(error))
    })

  })


})

