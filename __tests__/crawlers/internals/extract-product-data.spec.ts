import { describe, expect, jest, test } from '@jest/globals'
import { load } from 'cheerio'
import { productPageMock } from '../../../__mocks__/p/product-page.mock'
import extractProductData from '../../../src/crawlers/internals/extract-product-data'

describe('private function extractProductData', () => {
  test('selects & extracts schema.org/Product JSON from a webpage', () => {
    expect(extractProductData).toBeDefined()

    const $ = load(productPageMock)
    const product = extractProductData($)

    expect(product).toBeDefined()
    expect(product.sku).toBeDefined()
    expect(product.sku).toBe('1009621516')
    expect(product.aggregateRating).toBeDefined()
  })
})
