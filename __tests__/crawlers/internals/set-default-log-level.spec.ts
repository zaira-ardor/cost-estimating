import { afterEach, beforeEach, describe, expect, test } from '@jest/globals'
import { log } from 'crawlee'
import { setDefaultLogLevel } from '../../../src/crawlers'

describe('setDefaultLogLevel is a convenience function that', () => {
  beforeEach(() => {
    log.setLevel(log.LEVELS.INFO)
  })
  afterEach(() => {
    log.setLevel(log.LEVELS.INFO)
  })
  test('sets crawlee\'s overall log level', () => {
    expect(log.getLevel()).toBe(log.LEVELS.INFO)
    setDefaultLogLevel(log.LEVELS.WARNING)
    expect(log.getLevel()).toBe(log.LEVELS.WARNING)
  })
})
