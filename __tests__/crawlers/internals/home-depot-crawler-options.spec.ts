import { describe, expect, jest, test } from '@jest/globals'
import { type CheerioCrawlerOptions } from '@crawlee/cheerio'
import { HomeDepotCrawlerOptions } from '../../../src/crawlers/'

describe('class HomeDepotCrawlerOptions', () => {
  const withValue = {
    THIRTY: 30,
    SIXTY: 60
  }
  test('extends CheerioCrawlerOptions', () => {
    const config = new HomeDepotCrawlerOptions()
    expect(config).toHaveProperty(
      'requestHandlerTimeoutSecs',
      withValue.THIRTY
    )
  })
  test('static method load creates a HomeDepotCrawlerOptions instance', () => {
    const config = HomeDepotCrawlerOptions.load({
      requestHandlerTimeoutSecs: withValue.SIXTY
    })
    expect(config).toBeInstanceOf(HomeDepotCrawlerOptions)
    expect(config).toHaveProperty(
      'requestHandlerTimeoutSecs',
      withValue.SIXTY
    )
  })
})
