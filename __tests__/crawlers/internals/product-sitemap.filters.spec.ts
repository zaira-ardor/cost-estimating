import { describe, expect, test } from '@jest/globals'
import { type CheerioCrawlingContext } from '@crawlee/cheerio'
import { Request } from 'crawlee'
import isProductPageRequest from '../../../src/crawlers/internals/product-sitemap.filters'

describe(`isProductPageRequest`, () => {
  const url =
    'https://www.homedepot.com/p/Nameeks-General-Hotel-Freestanding-Soap-Dispenser-in-Chrome-Nameeks-NCB84/322430366'
  const xml = 'https://www.homedepot.com/sitemap/P/PIPs/59H/059-035-0.xml'

  test('determines whether a URL is a Home Depot product page', () => {
    expect(isProductPageRequest(url)).toBe(true)
    expect(isProductPageRequest(xml)).toBe(false)
  })
})

