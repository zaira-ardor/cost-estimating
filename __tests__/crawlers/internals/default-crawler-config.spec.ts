import { describe, expect, jest, test } from '@jest/globals'
import { Configuration, log } from 'crawlee'
import { defaultCrawlerConfig } from '../../../src/crawlers'
import crawleeJson from '../../../src/crawlee.json'

describe('defaultCrawlerConfig provides', () => {
  defaultCrawlerConfig.set('logLevel', log.LEVELS.INFO)
  describe('the default crawler config value for', () => {
    test('availableMemoryRatio', () => {
      expect(defaultCrawlerConfig.get('availableMemoryRatio')).toBe(
        crawleeJson.availableMemoryRatio
      )
    })
    test('chromeExecutablePath', () => {
      expect(defaultCrawlerConfig.get('chromeExecutablePath')).toBeUndefined()
    })
    test('defaultBrowserPath', () => {
      expect(defaultCrawlerConfig.get('defaultBrowserPath')).toBeUndefined()
    })
    test('defaultDatasetId', () => {
      expect(defaultCrawlerConfig.get('defaultDatasetId')).toBe(
        crawleeJson.defaultDatasetId
      )
    })
    test('defaultKeyValueStoreId', () => {
      expect(defaultCrawlerConfig.get('defaultKeyValueStoreId')).toBe(
        crawleeJson.defaultKeyValueStoreId
      )
    })
    test('defaultRequestQueueId', () => {
      expect(defaultCrawlerConfig.get('defaultRequestQueueId')).toBe(
        crawleeJson.defaultRequestQueueId
      )
    })
    test('disableBrowserSandbox', () => {
      expect(defaultCrawlerConfig.get('disableBrowserSandbox')).toBeUndefined()
    })
    test('eventManager', () => {
      expect(defaultCrawlerConfig.get('eventManager')).toBeUndefined()
    })
    test('headless', () => {
      expect(defaultCrawlerConfig.get('headless')).toBe(crawleeJson.headless)
    })
    test('inputKey', () => {
      expect(defaultCrawlerConfig.get('inputKey')).toBe(crawleeJson.inputKey)
    })
    test('logLevel', () => {
      expect(defaultCrawlerConfig.get('logLevel')).toBe(log.LEVELS.INFO)
    })
    test('maxUsedCpuRatio', () => {
      expect(defaultCrawlerConfig.get('maxUsedCpuRatio')).toBe(
        crawleeJson.maxUsedCpuRatio
      )
    })
    test('memoryMbytes', () => {
      expect(defaultCrawlerConfig.get('memoryMbytes')).toBeUndefined()
    })
    test('persistStateIntervalMillis', () => {
      expect(defaultCrawlerConfig.get('persistStateIntervalMillis')).toBe(
        crawleeJson.persistStateIntervalMillis
      )
    })
    test('persistStorage', () => {
      expect(defaultCrawlerConfig.get('persistStorage')).toBe(
        crawleeJson.persistStorage
      )
    })
    test('purgeOnStart', () => {
      expect(defaultCrawlerConfig.get('purgeOnStart')).toBe(
        crawleeJson.purgeOnStart
      )
    })
    test('storageClient', () => {
      expect(defaultCrawlerConfig.get('storageClient')).toBeUndefined()
    })
    test('storageClientOptions', () => {
      expect(defaultCrawlerConfig.get('storageClientOptions')).toEqual({})
    })
    test('systemInfoIntervalMillis', () => {
      expect(defaultCrawlerConfig.get('systemInfoIntervalMillis')).toBe(
        crawleeJson.systemInfoIntervalMillis
      )
    })
    test('xvfb', () => {
      expect(defaultCrawlerConfig.get('xvfb')).toBeUndefined()
    })
  })

})
