import { describe, expect, jest, test } from '@jest/globals'
import { Configuration } from 'crawlee'
import HomeDepotProductExtractor, {
  constants,
  defaultCrawlerConfig,
  defaultCrawlerRunOptions,
  HomeDepotCrawler,
  HomeDepotCrawlerOptions,
  setDefaultLogLevel
} from '../../src/crawlers'

describe('The crawlers module', () => {
  describe('exports', () => {
    test('constants', () => {
      expect(constants).toBeDefined()
    })
    test('defaultCrawlerConfig', () => {
      expect(defaultCrawlerConfig).toBeDefined()
      expect(defaultCrawlerConfig).toBeInstanceOf(Configuration)
    })
    test('defaultCrawlerRunOptions', () => {
      expect(defaultCrawlerRunOptions).toBeDefined()
    })
    test('HomeDepotCrawler', () => {
      expect(HomeDepotCrawler).toBeDefined()
    })
    test('HomeDepotCrawlerOptions', () => {
      expect(HomeDepotCrawlerOptions).toBeDefined()
    })
    test('HomeDepotProductExtractor', () => {
      expect(HomeDepotProductExtractor).toBeDefined()
    })
    test('setDefaultLogLevel', () => {
      expect(setDefaultLogLevel).toBeDefined()
    })
  })
})
