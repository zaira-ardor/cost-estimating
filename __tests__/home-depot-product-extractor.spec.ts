import { describe, expect, test } from '@jest/globals'
import HomeDepotProductExtractor, {
  constants,
  defaultCrawlerConfig,
  defaultCrawlerRunOptions,
  HomeDepotCrawler,
  homeDepotCrawlerOptions,
  HomeDepotCrawlerOptions,
  homeDepotRouter,
  ProductFulfillmentAdaptor,
  setDefaultLogLevel
} from '../src'

describe('The "home-depot-product-extractor" module exports', () => {
  test('HomeDepotProductExtractor', () => {
    expect(HomeDepotProductExtractor).toBeDefined()
  })
  test('constants', () => {
    expect(constants).toBeDefined()
  })
  test('defaultCrawlerConfig', () => {
    expect(defaultCrawlerConfig).toBeDefined()
  })
  test('defaultCrawlerRunOptions', () => {
    expect(defaultCrawlerRunOptions).toBeDefined()
  })
  test('HomeDepotCrawler', () => {
    expect(HomeDepotCrawler).toBeDefined()
  })
  test('homeDepotCrawlerOptions', () => {
    expect(homeDepotCrawlerOptions).toBeDefined()
  })
  test('HomeDepotCrawlerOptions', () => {
    expect(HomeDepotCrawlerOptions).toBeDefined()
  })
  test('homeDepotRouter', () => {
    expect(homeDepotRouter).toBeDefined()
  })
  test('ProductFulfillmentAdaptor', () => {
    expect(ProductFulfillmentAdaptor).toBeDefined()
  })
  test('setDefaultLogLevel', () => {
    expect(setDefaultLogLevel).toBeDefined()
  })
})
