import { describe, expect, test } from '@jest/globals'
import { type Offer, type Product, type QuantitativeValue, type WithContext } from 'schema-dts'
import createWithContextProduct from '../../../../__mocks__/utils/create-with-context-product'
import impactWrenchMock from '../../../../__mocks__/p/impact-wrench.mock.json'
import productClientOnlyProduct from '../../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'
import setAvailableDeliveryMethod from '../../../../src/transformers/product-fulfillment-adaptor/internals/set-available-delivery-method'

describe('function setAvailableDeliveryMethod', () => {
  test('adds the "setAvailableDeliveryMethod" property to product.offers', () => {
    const product = createWithContextProduct(impactWrenchMock) as Product
    setAvailableDeliveryMethod(product, productClientOnlyProduct)
    // @ts-expect-error
    expect(product.offers!.availableDeliveryMethod).toBeInstanceOf(Array)
    // @ts-expect-error
    expect(product.offers!.availableDeliveryMethod.length).toBe(2)
  })
})

