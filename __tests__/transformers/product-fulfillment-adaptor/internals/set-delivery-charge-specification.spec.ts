import { describe, expect, test } from '@jest/globals'
import { type Offer, type Product, type QuantitativeValue, type WithContext } from 'schema-dts'
import createWithContextProduct from '../../../../__mocks__/utils/create-with-context-product'
import impactWrenchMock from '../../../../__mocks__/p/impact-wrench.mock.json'
import productClientOnlyProduct from '../../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'
import setDeliveryChargeSpecification from '../../../../src/transformers/product-fulfillment-adaptor/internals/set-delivery-charge-specification'

describe('function setDeliveryChargeSpecification', () => {
  test('adds a "priceSpecification" property to product.offers', () => {
    const product = createWithContextProduct(impactWrenchMock) as Product
    setDeliveryChargeSpecification(product, productClientOnlyProduct)
    // @ts-expect-error
    expect(product.offers!.priceSpecification).toBeInstanceOf(Array)
  })
})

