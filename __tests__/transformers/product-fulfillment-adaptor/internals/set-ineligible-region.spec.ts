import { describe, expect, test } from '@jest/globals'
import { type Offer, type Product, type QuantitativeValue, type WithContext } from 'schema-dts'
import setIneligibleRegion from '../../../../src/transformers/product-fulfillment-adaptor/internals/set-ineligible-region'
import createWithContextProduct from '../../../../__mocks__/utils/create-with-context-product'
import enrichedProductMock from '../../../../__mocks__/transformers/enriched-product.mock.json'
import impactWrenchMock from '../../../../__mocks__/p/impact-wrench.mock.json'
import productClientOnlyProduct from '../../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'

describe('function setIneligibleRegion', () => {
  test('adds the "ineligibleRegion" property to product.offers', () => {
    const product = createWithContextProduct(impactWrenchMock) as Product
    setIneligibleRegion(product, productClientOnlyProduct)
    // @ts-expect-error
    expect(product.offers?.ineligibleRegion).toBeInstanceOf(Array)
    const FIVE_STATES = 5
    // @ts-expect-error
    expect(product.offers?.ineligibleRegion.length).toBe(FIVE_STATES)
  })
})
