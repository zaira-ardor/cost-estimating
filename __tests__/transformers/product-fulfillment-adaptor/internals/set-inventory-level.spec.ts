import { describe, expect, test } from '@jest/globals'
import { type Offer, type Product, type QuantitativeValue, type WithContext } from 'schema-dts'
import setInventoryLevel from '../../../../src/transformers/product-fulfillment-adaptor/internals/set-inventory-level'
import createWithContextProduct from '../../../../__mocks__/utils/create-with-context-product'
import enrichedProductMock from '../../../../__mocks__/transformers/enriched-product.mock.json'
import impactWrenchMock from '../../../../__mocks__/p/impact-wrench.mock.json'
import productClientOnlyProduct from '../../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'

describe('setInventoryLevel', () => {
  test('adds the property "inventoryLevel" on product.offers', () => {
    const product = createWithContextProduct(impactWrenchMock) as Product
    // @ts-expect-error
    expect(product.offers?.inventoryLevel).toBeUndefined()
    const enrichedProduct = setInventoryLevel(product, productClientOnlyProduct)
    // @ts-expect-error
    expect(product.offers?.inventoryLevel).toBeDefined()
    const QUANTITY = 8
    // @ts-expect-error
    expect(product.offers?.inventoryLevel).toBe(QUANTITY)
    // @ts-expect-error
    expect(enrichedProduct.offers?.inventoryLevel).toBe(QUANTITY)
  })
})
