import { describe, expect, test } from '@jest/globals'
import { type Offer, type Product, type QuantitativeValue, type WithContext } from 'schema-dts'
import createWithContextProduct from '../../../../__mocks__/utils/create-with-context-product'
import impactWrenchMock from '../../../../__mocks__/p/impact-wrench.mock.json'
import productClientOnlyProduct from '../../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'
import setAvailableAtOrFrom from '../../../../src/transformers/product-fulfillment-adaptor/internals/set-available-at-or-from'

describe('function setAvailableAtOrFrom', () => {
  test('adds the property "availableAtOrFrom" to product.offers', () => {
    const product = createWithContextProduct(impactWrenchMock) as Product
    setAvailableAtOrFrom(product, productClientOnlyProduct)
    // @ts-expect-error
    expect(product.offers!.availableAtOrFrom).toBeInstanceOf(Array)
    // @ts-expect-error
    product.offers!.availableAtOrFrom.forEach((place: object) => {
      // @ts-expect-error
      expect(place['@type']).toBe('HomeAndConstructionBusiness')
      // @ts-expect-error
      expect(place.address.addressLocality).toBe('Detroit')
      // @ts-expect-error
      expect(place.address.addressRegion).toBe('MI')
      // @ts-expect-error
      expect(place.identifier).toBeDefined()
      // @ts-expect-error
      expect(place.image).toBeDefined()
      // @ts-expect-error
      expect(place.location).toBe('online')
      // @ts-expect-error
      expect(place.name).toBeDefined()
    })
  })
})

