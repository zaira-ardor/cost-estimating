import { beforeEach, describe, expect, test } from '@jest/globals'
import { ProductClientOnlyProductAdaptee } from '../../../src/transformers/product-fulfillment-adaptor/interfaces/product-client-only-product-adaptee.d'
import { ProductFulfillmentAdaptor } from '../../../src'
import { type Product, type WithContext } from 'schema-dts'
import adaptee from '../../../__mocks__/federation-gateway/graphql/productClientOnlyProduct.json'
import createWithContextProduct from '../../../__mocks__/utils/create-with-context-product'
import impactWrenchMock from '../../../__mocks__/p/impact-wrench.mock.json'

describe('class ProductFulfillmentAdaptor', () => {
  let product: WithContext<Product>
  let adaptor: ProductFulfillmentAdaptor

  beforeEach(() => {
    product = createWithContextProduct(impactWrenchMock)
    adaptor = new ProductFulfillmentAdaptor(product, adaptee)
  })

  describe('transforms a product by', () => {
    test('removing its "reviews" property', () => {
      expect(product).toStrictEqual(impactWrenchMock)
      expect(product.review).toBeDefined()
      adaptor.removeReviews()
      expect(product.review).toBeUndefined()
    })
    test('creating an "inventoryLevel" property on it', () => {
      // @ts-expect-error
      expect(product.offers?.inventoryLevel).toBeUndefined()
      const QTY = 8
      adaptor.setInventoryLevel()
      // @ts-expect-error
      expect(product.offers!.inventoryLevel).toBeDefined()
      // @ts-expect-error
      expect(product.offers!.inventoryLevel).toBe(QTY)
    })
    test('creating an "availableAtOrFrom" property', () => {
      adaptor.setAvailableAtOrFrom()
      // @ts-expect-error
      expect(product.offers!.availableAtOrFrom).toBeInstanceOf(Array)
    })
    test('creating an "availableDeliveryMethod" property', () => {
      adaptor.setAvailableDeliveryMethod()
      // @ts-expect-error
      expect(product.offers!.availableDeliveryMethod).toBeInstanceOf(Array)
    })
    test('creating a "priceSpecification" property', () => {
      adaptor.setDeliveryChargeSpecification()
      // @ts-expect-error
      expect(product.offers!.priceSpecification).toBeInstanceOf(Array)
    })
    test('creating an "ineligibleRegion" property', () => {
      adaptor.setIneligibleRegion()
      // @ts-expect-error
      expect(product.offers!.ineligibleRegion).toBeInstanceOf(Array)
    })
    test('calling all methods with the "adapt" method', () => {
      adaptor.adapt()
      // @ts-expect-error
      expect(product.offers!.availableAtOrFrom).toBeInstanceOf(Array)
      // @ts-expect-error
      expect(product.offers!.availableDeliveryMethod).toBeInstanceOf(Array)
      // @ts-expect-error
      expect(product.offers!.ineligibleRegion).toBeInstanceOf(Array)
      // @ts-expect-error
      expect(product.offers!.priceSpecification).toBeInstanceOf(Array)
      // @ts-expect-error
      expect(product.offers!.inventoryLevel).toBeDefined()
      expect(product.review).toBeUndefined()
    })
  })
})
