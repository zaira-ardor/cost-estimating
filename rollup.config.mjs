import { defineConfig } from 'rollup'
import pkg from './package.json' assert { type: 'json' }
import commonjs from '@rollup/plugin-commonjs'
import jsonPlugin from '@rollup/plugin-json'
import nodeResolve from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'

const config = defineConfig({
  external: Object.keys(pkg.dependencies),
  input: 'src/crawlers/index.ts',
  output: {
    compact: true,
    file: 'dist/hd-product-extractor.es.js',
    format: 'module',
    globals: {
      events: 'node:events'
    },
    name: 'hd-product-extractor'
  },
  plugins: [
    commonjs(),
    jsonPlugin(),
    nodeResolve({
      moduleDirectories: [
        'node_modules'
      ],
      preferBuiltins: true
    }),
    typescript({
      include: ['**/*.ts', '**/*.tsx',],
      compilerOptions: {
        declaration: false,
        declarationDir: undefined,
        declarationMap: false,
        outDir: 'dist',
        removeComments: true
      },
      outputToFilesystem: true,
      tsconfig: './tsconfig.json'
    })
  ],
  treeshake: true
})

export default config
