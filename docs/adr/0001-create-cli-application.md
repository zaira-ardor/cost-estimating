# 1. Create CLI application

> Crawl, extract & export products from the <abbr title="Command Line Interface">CLI</abbr>.

![Decided on date][octicon-calendar] <time datetime="2024-03-05">2024-03-05</time>

## Status

| Status                           | Provision                     | Category                                                                    |
| -------------------------------- | ----------------------------- | --------------------------------------------------------------------------- |
| ![Accepted][adr-accepted-status] | ![Trial][adr-trial-provision] | ![Languages & frameworks][tr-lang-frameworks]<br>![Platforms][tr-platforms] |

## Context

[`crawlee`![external link][octicon-link-external]](https://crawlee.dev/) stores the [`schema.org/Product`![external link][octicon-link-external]](https://schema.org/Product) data that it finds on webpages in
the directory `<project-root>/storage/datasets/default/` as enumerated `.json` files. We need to
make the resulting list of [`schema.org/Products`![external link][octicon-link-external]](https://schema.org/Product) available to others for
_loading_ and _transformation_.

### Stakeholders

1. @zaira-ardor-llc
1. @gregswindle

### Concerns

1. **Encryption**: should we encrypt the dataset of `Products`?

   > > > _Answer_:

   No. `Product` data are publicly available, albeit embedded
   in HTML documents.

   > > >

2. **Password/token protection**: should we provide a "password"
   option, i.e., in order to view the `Product` files, one must
   first successfully enter a password?

   > > > _Answer_:

   No. `Product` data are publicly available, albeit embedded
   in HTML documents. If we expose this `extractor` as a web
   service, however, consumption of the service would require
   an `API_TOKEN`.

   > > >

### Stakeholder-concern table

Table 2: Stakeholder-concern traceability.

|   Stakeholder    | Concern 1 | Concern 2 |
| :--------------: | :-------: | :-------: |
| @zaira-ardor-llc |     -     |     -     |
|   @gregswindle   |     -     |     -     |

## Decision

We will export _home-depot-product-extractor_ as a Node.js module. Once published,
we will add _home-depot-product-extractor_ as a dependency to a new command-line
application (e.g., _home-depot-product-extractor-cli_).

We will use a project scaffolding tool such as [Scaffold CLI![external link][octicon-link-external]](https://www.npmjs.com/package/@scaffold.sh/cli) to create the application.

## Consequences

1. We will publish and maintain a <abbr title="Command Line Interface">CLI</abbr> application.
2. The <abbr title="Command Line Interface">CLI</abbr> app will use _home-depot-product-extractor_
   as a production dependency.
3. The <abbr title="Command Line Interface">CLI</abbr> app will deflate all products with `zip`.
4. The <abbr title="Command Line Interface">CLI</abbr> will include an `--export` argument so that
   consumers/users can declare where the `hd-products.zip` should be saved (on the local file system).

<!-- ⛔️ Do not remove this line or anything under it ⛔️ -->

<!-- Links ==>

[schema-org-product]: https://schema.org/Product

<!-- ADR workflow stage badges -->

[adr-process-proposed]: https://flat.badgen.net/badge/ADR/→+🗣+proposed/92B9C9 'Architecture decision proposal stage.'
[adr-process-vote]: https://flat.badgen.net/badge/ADR/→+🗳+vote/92B9C9 'Voting underway.'
[adr-process-resolved]: https://flat.badgen.net/badge/ADR/→+🏁+resolved/92B9C9 'Finalized architecture decisions.'

<!-- ADR status badges -->

[adr-accepted-status]: https://flat.badgen.net/badge/ADR/👍🏻accepted/242423 'An architecture decision that has been approved, with provision.'
[adr-proposed-status]: https://flat.badgen.net/badge/ADR/🗣proposed/92B9C9 'An architecture decision nominated for acceptance or rejection.'
[adr-rejected-status]: https://flat.badgen.net/badge/ADR/✖️rejected/DA2C38 'An unwanted or infeasible architecture decision.'
[adr-deprecated-status]: https://flat.badgen.net/badge/ADR/⛔️deprecated/F0B719 'This architecture decision has been withdrawn in favor of a newer alternative.'
[adr-supercedes-status]: https://flat.badgen.net/badge/ADR/➥supercedes/D49F0C 'A newer decision that replaces a deprecated ADR.'

<!-- ADR provision ("ring") badges -->

[adr-adopt-provision]: https://flat.badgen.net/badge/✓/adopt/B7B5CF "Represents something where there's no doubt that it's proven and mature for use."

[adr-trial-provision]: https://flat.badgen.net/badge/🎛/trial/B7B5CF "Ready for use, but not as completely proven as those in the \"adopt\" provision."
[adr-assess-provision]: https://flat.badgen.net/badge/💭/assess/B7B5CF "Decisions to look at closely, but not necessarily trial yet — unless you think they would be a particularly good fit for you."
[adr-hold-provision]: https://flat.badgen.net/badge/⚠️/hold/B7B5CF "For things that, even though they may be accepted in the industry, we haven't had a good experience with."

<!-- TechRadar category ("quadrant") badges -->

[tr-lang-frameworks]: https://flat.badgen.net/badge/◶/languages%20%26%20frameworks/f16179
[tr-platforms]: https://flat.badgen.net/badge/◵/platforms/cd840b
[tr-techniques]: https://flat.badgen.net/badge/◴/techniques/47a0ad
[tr-tools]: https://flat.badgen.net/badge/◷/tools/6b9e78
[architecture-decisions-guide]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Governance/Architecture-Decisions
[citations]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Style-Guides/Citations-(APA) 'Go to our Wiki page for Citation Guidelines.'
[octicon-calendar]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg
[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg
[octicon-question]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg
