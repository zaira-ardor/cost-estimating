import constants from './internals/constants'
import defaultCrawlerConfig from './internals/default-crawler-config'
import defaultCrawlerRunOptions from './internals/default-crawler-run-options'
import HomeDepotCrawler from './internals/home-depot-crawler'
import homeDepotCrawlerOptions, { HomeDepotCrawlerOptions } from './internals/home-depot-crawler-options'
import HomeDepotProductExtractor from './internals/home-depot-product-extractor'
import homeDepotRouter from './internals/home-depot-router'
import setDefaultLogLevel from './internals/set-default-log-level'
import ProductFulfillmentAdaptor from '../transformers/product-fulfillment-adaptor'

// setDefaultLogLevel(defaultCrawlerConfig.get('logLevel'))

export default HomeDepotProductExtractor
export {
  constants,
  defaultCrawlerConfig,
  defaultCrawlerRunOptions,
  HomeDepotCrawler,
  homeDepotCrawlerOptions,
  HomeDepotCrawlerOptions,
  homeDepotRouter,
  ProductFulfillmentAdaptor,
  setDefaultLogLevel
}
