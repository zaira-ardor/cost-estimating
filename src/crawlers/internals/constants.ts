enum sitemaps {
  brands = 'https://www.homedepot.com/sitemap/B/Brands.xml',
  categories = 'https://www.homedepot.com/sitemap/Cat.xml',
  homeServices = 'https://www.homedepot.com/sitemap/HomeServices.xml',
  main = 'https://www.homedepot.com/sitemap/main.xml',
  productLists = 'https://www.homedepot.com/sitemap/B/PLPs.xml',
  products = 'https://www.homedepot.com/sitemap/P/PIPs.xml',
  stores = 'https://www.homedepot.com/sitemap/PPS.xml'
}

enum selectors {
  product = 'script[id$=productStructureData]'
}

enum robotsTxt {
  url = 'https://www.homedepot.com/robots.txt'
}

class Constants {
  readonly selectors = selectors
  readonly urls = {
    robots: robotsTxt.url,
    sitemaps
  }
}

const constants = new Constants()

export default constants
