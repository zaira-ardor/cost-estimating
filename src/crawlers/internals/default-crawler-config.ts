import { Configuration } from 'crawlee'

const defaultCrawlerConfig: Configuration =
  (Configuration.getGlobalConfig() ?? new Configuration())

export default defaultCrawlerConfig
