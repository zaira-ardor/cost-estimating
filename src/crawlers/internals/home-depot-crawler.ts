import { CheerioCrawler, type Configuration } from 'crawlee'
import defaultCrawlerConfig from './default-crawler-config'
import homeDepotCrawlerOptions, { type HomeDepotCrawlerOptions } from './home-depot-crawler-options'

/* c8 ignore start */

/**
 * Create a crawler dedicated to Home Depot's
 * produfts.
 *
 * ⚠︎ NOTE: Despite having tests that prove this class's
 * features, I cannot get jest's coverage strategy
 * (v8 | instanbul) to recognize it.
 * Run npm test to confirm.
 *
 * @date 2024-03-16
 * @class HomeDepotCrawler
 * @extends {CheerioCrawler}
 */
class HomeDepotCrawler extends CheerioCrawler {
  constructor (
    crawlerOptions: HomeDepotCrawlerOptions = homeDepotCrawlerOptions,
    crawlerConfig: Configuration = defaultCrawlerConfig
  ) {
    super(crawlerOptions, crawlerConfig)
  }

  static factory (
    crawlerOptions?: HomeDepotCrawlerOptions,
    crawlerConfig?: Configuration
  ): HomeDepotCrawler {
    return new HomeDepotCrawler(crawlerOptions, crawlerConfig)
  }
}
/* c8 ignore end */

export default HomeDepotCrawler
