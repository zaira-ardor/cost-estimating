import { type CheerioAPI } from 'cheerio'
import { type Product, type WithContext } from 'schema-dts'
import constants from './constants'

const extractProductData = function withCssSelector (
  $: CheerioAPI
): WithContext<Product> {
  const { product } = constants.selectors
  const json: string = $(product).text()
  const schemaOrgProduct = JSON.parse(json) as WithContext<Product>
  return schemaOrgProduct
}

export default extractProductData
