import {
  type Configuration,
  type CrawlerRunOptions,
  type FinalStatistics,
  type Request,
  type RequestOptions,
  RobotsFile,
  Sitemap
} from 'crawlee'
import constants from './constants'
import defaultCrawlerConfig from './default-crawler-config'
import HomeDepotCrawler from './home-depot-crawler'
import homeDepotCrawlerOptions, { type HomeDepotCrawlerOptions } from './home-depot-crawler-options'

class HomeDepotProductExtractor {
  public readonly config: Configuration
  public readonly crawler: HomeDepotCrawler
  public readonly options: HomeDepotCrawlerOptions
  public readonly robots: RobotsFile
  public readonly sitemap: Sitemap
  public url: string

  constructor (
    url?: string,
    options?: HomeDepotCrawlerOptions,
    config?: Configuration
  ) {
    this.config = config ?? defaultCrawlerConfig
    this.options = options ?? homeDepotCrawlerOptions
    this.crawler = new HomeDepotCrawler(this.options, this.config)
    this.sitemap = new Sitemap([constants.urls.sitemaps.products])
    this.url = url ?? constants.urls.sitemaps.products
    this.robots = RobotsFile.from(this.url, '')
  }

  async run (
    requests?: Array<string | Request | RequestOptions>,
    options?: CrawlerRunOptions): Promise<FinalStatistics> {
    const reqs = requests ?? [this.url]
    return await this.crawler.run(reqs, options)
  }
}

export default HomeDepotProductExtractor
