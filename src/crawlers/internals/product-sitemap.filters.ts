const match = {
  product: {
    page: /\/p\/.*\/\d{9,20}/gm
  }
}

const isProductPageRequest = (url: string): boolean =>
  match.product.page.test(url)

export default isProductPageRequest
