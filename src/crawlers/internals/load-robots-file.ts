/* c8 ignore start */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Buffer } from 'node:buffer'
import https from 'node:https'
import { URL } from 'node:url'
import constants from './constants'
import handleCatchBlock from './handle-catch-block'

const maxRedirects = 20
const method = 'GET'

interface RequestsOptions {
  hostname?: string
  maxRedirects: number
  method: string
  path?: string
}

const robotsFileRequestOptions = {
  method,
  maxRedirects
} satisfies RequestsOptions

const loadRobotsFile =
  async function req (
    url: string = constants.urls.robots,
    options: RequestsOptions = robotsFileRequestOptions): Promise<string> {
    return await new Promise((resolve, reject) => {
      const robotsUrl: URL = new URL(url)
      const opts: RequestsOptions = {
        hostname: robotsUrl.hostname,
        path: robotsUrl.pathname,
        ...options
      }
      // Postman v10.24.7 generated the following source code
      const req = https.request(opts, (res) => {
        const chunks: Buffer[] = []
        res.on('data', (chunk: Buffer) => {
          chunks.push(chunk)
        })

        res.on('end', () => {
          const body = Buffer.concat(chunks)
          resolve(body.toString())
        })

        res.on('error', (error) => {
          const reason = handleCatchBlock(error)
          reject(reason)
        })
      })

      req.end()
    })
  }

export default loadRobotsFile
/* c8 ignore end */
