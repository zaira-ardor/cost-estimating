import { type AnyNode, type BasicAcceptedElems } from 'cheerio'
import { type CheerioCrawlingContext } from '@crawlee/cheerio'
import { createCheerioRouter, Dataset } from 'crawlee'
import { type Product, type WithContext } from 'schema-dts'
import extractProductData from './extract-product-data'
import isProductPageRequest from './product-sitemap.filters'

const addRequestsFromSitemap = (ctx: CheerioCrawlingContext): void => {
  const hrefs = ctx.$('loc')
    .map((_: unknown, elem: BasicAcceptedElems<AnyNode>) => ctx.$(elem).text())
    .get()
  void ctx.addRequests(hrefs)
}

const storeProductData = async (ctx: CheerioCrawlingContext): Promise<void> => {
  const product: WithContext<Product> = extractProductData(ctx.$)
  await Dataset.pushData(product)
}

const homeDepotRouter = createCheerioRouter<CheerioCrawlingContext>()

homeDepotRouter.addDefaultHandler(async (ctx: CheerioCrawlingContext) => {
  await ctx.enqueueLinks({
    globs: [
      '**/sitemap/P/PIPs/**/*.xml', // Product sitemaps
      '**/p/**/*' // Product URLs
    ]
  })
  const uri = ctx.request.loadedUrl ?? ctx.request.url
  if (isProductPageRequest(uri)) {
    await storeProductData(ctx)
  } else {
    addRequestsFromSitemap(ctx)
  }
})

export default homeDepotRouter
