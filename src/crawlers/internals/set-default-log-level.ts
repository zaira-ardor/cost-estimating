import { log } from 'crawlee'

const setDefaultLogLevel = (level: number = log.LEVELS.INFO): number => {
  log.setLevel(level)
  return level
}

export default setDefaultLogLevel
