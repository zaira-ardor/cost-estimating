import { type CrawlerRunOptions } from 'crawlee'

const ONE_THOUSAND = 1000
/**
 * A configuration options object with default values that determine
 * how requests should be executed.
 *
 * @category utilities:options
 * @property {number} [batchSize=ONE_THOUSAND] The number of requests handle
 * at a time.
 *
 * @property {boolean} [forefront=false] If set to true:
 *
 * - while adding the request to the queue: the request will be added to the
 * foremost position in the queue.
 *
 * - while reclaiming the request: the request will be placed to the beginning
 * of the queue, so that it's returned in the next call to RequestQueue.
 * fetchNextRequest. By default, it's put to the end of the queue.
 *
 * @property {boolean} [purgeRequestQueue=true] Whether to purge the
 * RequestQueue before running the crawler again. Defaults to true, so it is
 * possible to reprocess failed requests. When disabled, only new requests will
 * be considered. Note that even a failed request is considered as handled.
 *
 * @property {number} [waitBetweenBatchesMillis=ONE_THOUSAND] The number of
 * milliseconds to pause between batches.
 *
 * @property {boolean} [waitForAllRequestsToBeAdded=false] Whether to wait for
 * all the provided requests to be added, instead of waiting just for the
 * initial batch of up to batchSize.
 *
 * @memberOf defaultCrawlerRunOptions
 */
const defaultCrawlerRunOptions: CrawlerRunOptions = {
  batchSize: ONE_THOUSAND,
  forefront: false,
  purgeRequestQueue: true,
  waitBetweenBatchesMillis: ONE_THOUSAND,
  waitForAllRequestsToBeAdded: false
} satisfies CrawlerRunOptions

export default defaultCrawlerRunOptions
