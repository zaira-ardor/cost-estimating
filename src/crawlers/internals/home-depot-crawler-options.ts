import { type BasicCrawler, type CrawlerExperiments } from 'crawlee'
import {
  type AutoscaledPoolOptions,
  type BasicCrawlingContext,
  type CheerioCrawlerOptions,
  type CheerioCrawlingContext,
  type ErrorHandler,
  type InternalHttpHook,
  type ProxyConfiguration,
  type RequestHandler,
  type RequestProvider,
  type RequestList,
  type SessionPoolOptions,
  type StatisticsOptions,
  type StatusMessageCallback
} from '@crawlee/cheerio'
import homeDepotRouter from './home-depot-router'

const DELAY_SECONDS = 0
const FIFTY = 50
const MILLISECOND = 1000
const TEN = 10
const THIRTY = 30
const TWICE = 2

class HomeDepotCrawlerOptions implements CheerioCrawlerOptions {
  /**
   * An array of additional HTTP response Status Codes to be treated as errors.
   * By default, status codes >= 500 trigger errors.
   *
   * @type {number[]}
   * @memberof HomeDepotCrawlerOptions
   */
  public additionalHttpErrorStatusCodes?: number[]
  /**
   * An array of MIME types you want the crawler to load and process. By
   * default, only text/html and application/xhtml+xml MIME types are supported.
   *
   * @type {string[]}
   * @memberof HomeDepotCrawlerOptions
   * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
   */
  public additionalMimeTypes?: string[]
  /**
   * Custom options passed to the underlying `AutoscaledPool` constructor.
   *
   * ⓘ NOTE: The `runTaskFunction` and `isTaskReadyFunction` options are
   * provided by the crawler and cannot be overridden. However, we can provide
   * a custom implementation of `isFinishedFunction`.
   *
   * @type {AutoscaledPoolOptions}
   * @memberof HomeDepotCrawlerOptions
   */
  public autoscaledPoolOptions?: AutoscaledPoolOptions
  /**
   * User-provided function that allows modifying the request object before it
   * gets retried by the crawler. It's executed before each retry for the
   * requests that failed less than `maxRequestRetries` times.
   *
   * The function receives the `BasicCrawlingContext` as the first argument,
   * where the `request` corresponds to the `request` to be retried.
   * The Second argument is the `Error` instance that represents the last error
   * thrown during processing of the request.
   *
   * @type {ErrorHandler<CheerioCrawlingContext>}
   * @memberof HomeDepotCrawlerOptions
   */
  public errorHandler?: ErrorHandler<CheerioCrawlingContext>
  /**
   * Enables experimental features of Crawlee, which can alter the behavior of
   * the crawler.
   *
   * ⚠︎ WARNING: these options are not guaranteed to be stable and may change
   * or be removed at any time.
   *
   * @type {CrawlerExperiments}
   * @memberof HomeDepotCrawlerOptions
   */
  public experiments?: CrawlerExperiments
  /**
   * A function to handle requests that failed more than `maxRequestRetries`
   * times.
   *
   * The function receives the `BasicCrawlingContext` as the first argument,
   * where the request corresponds to the failed request. The second argument is
   * the `Error` instance that represents the last error thrown during
   * processing of the request.
   *
   * @type {ErrorHandler<CheerioCrawlingContext>}
   * @memberof HomeDepotCrawlerOptions
   */
  public failedRequestHandler?: ErrorHandler<CheerioCrawlingContext>
  /**
   * By default this crawler will extract correct encoding from the HTTP
   * response headers. Use forceResponseEncoding to force a certain encoding,
   * disregarding the response headers. To only provide a default for missing
   * encodings, use `HttpCrawlerOptions.suggestResponseEncoding`.
   *
   * @type {string}
   * @memberof HomeDepotCrawlerOptions
   */
  public forceResponseEncoding?: string
  /**
   * An alias for `HttpCrawlerOptions.requestHandler`.
   *
   * ⚠︎ NOTE: Will be removed soon: use `requestHandler` instead.
   *
   * @type {RequestHandler<CheerioCrawlingContext>}
   * @memberof HomeDepotCrawlerOptions
   * @deprecated
   */
  public handlePageFunction?: RequestHandler<CheerioCrawlingContext>
  /**
   * An array of HTTP response Status Codes to be excluded from error
   * consideration. By default, status codes >= 500 trigger errors.
   *
   * @type {number[]}
   * @memberof HomeDepotCrawlerOptions
   */
  public ignoreHttpErrorStatusCodes?: number[]
  /**
   * If set to true, SSL certificate errors will be ignored.
   *
   * @type {boolean}
   * @memberof HomeDepotCrawlerOptions
   * @default true
   */
  public ignoreSslErrors?: boolean = true
  /**
   * Allows to keep the crawler alive even if the `RequestQueue` gets empty. By
   * default, the `crawler.run()` will resolve once the queue is empty. With
   * `keepAlive: true` it will keep running, waiting for more requests to come.
   * Use `crawler.teardown()` to exit the crawler.
   *
   * @type {boolean}
   * @memberof HomeDepotCrawlerOptions
   * @default false
   */
  public keepAlive?: boolean = false
  /**
   * Sets the maximum concurrency (parallelism) for the crawl. Shortcut for the * AutoscaledPool `maxConcurrency` option.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default 50
   */
  public maxConcurrency?: number = FIFTY
  /**
   * Indicates how many times the request is retried if `requestHandler` fails.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default 2
   */
  public maxRequestRetries?: number = TWICE
  /**
   * Maximum number of pages that the crawler will open. The crawl will stop
   * when this limit is reached. This value should always be set in order to
   * prevent infinite loops in misconfigured crawlers.
   *
   * ⓘ NOTE: In cases of parallel crawling, the actual number of pages visited
   * might be slightly higher than this value.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default Number.POSITIVE_INFINITY
   */
  public maxRequestsPerCrawl?: number = Number.POSITIVE_INFINITY
  /**
   * The maximum number of requests per minute the crawler should run. By
   * default, this is set to `Infinity`, but we can pass any positive, non-zero
   * integer. Shortcut for the AutoscaledPool `maxTasksPerMinute` option.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default Number.POSITIVE_INFINITY
   */
  public maxRequestsPerMinute?: number = Number.POSITIVE_INFINITY
  /**
   * Maximum number of session rotations per request. The crawler will
   * automatically rotate the session in case of a proxy error or if it gets
   * blocked by the website.
   *
   * The session rotations are not counted towards the `maxRequestRetries`
   * limit.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default 10
   */
  public maxSessionRotations?: number = TEN
  /**
   * Sets the minimum concurrency (parallelism) for the crawl. Shortcut for the
   * AutoscaledPool minConcurrency option.
   *
   * ⚠︎ WARNING: If you set this value too high with respect to the available
   * system memory and CPU, our crawler will run extremely slow or crash. If
   * not sure, it's better to keep the default value and the concurrency will
   * scale up automatically.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   */
  public minConcurrency?: number
  /**
   * Timeout in which the HTTP request to the resource needs to finish, given
   * in seconds.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   */
  public navigationTimeoutSecs?: number
  /**
   * Automatically saves cookies to Session. Works only if Session Pool is used.
   *
   * It parses cookie from response "set-cookie" header saves or updates
   * cookies for session and once the session is used for next request. It
   * passes the "Cookie" header to the request with the session cookies.
   *
   * @type {boolean}
   * @memberof HomeDepotCrawlerOptions
   * @default false
   */
  public persistCookiesPerSession?: boolean = false
  /**
   * Async functions that are sequentially evaluated after the navigation. Good
   * for checking if the navigation was successful. The function accepts
   * crawlingContext as the only parameter.
   *
   * @type {Array<InternalHttpHook<CheerioCrawlingContext>>}
   * @memberof HomeDepotCrawlerOptions
   * @see https://crawlee.dev/api/cheerio-crawler/interface/CheerioCrawlerOptions#postNavigationHooks
   */
  public postNavigationHooks?: Array<InternalHttpHook<CheerioCrawlingContext>>
  /**
   * Async functions that are sequentially evaluated before the navigation.
   * Good for setting additional cookies or browser properties before
   * navigation. The function accepts two parameters, `crawlingContext` and
   * `gotOptions`, which are passed to the requestAsBrowser() function the
   * crawler calls to navigate.
   *
   * @type {Array<InternalHttpHook<CheerioCrawlingContext>>}
   * @memberof HomeDepotCrawlerOptions
   * @see https://crawlee.dev/api/cheerio-crawler/interface/CheerioCrawlerOptions#preNavigationHooks
   */
  public preNavigationHooks?: Array<InternalHttpHook<CheerioCrawlingContext>>
  /**
   * If set, this crawler will be configured for all connections to use Apify
   * Proxy or your own Proxy URLs provided and rotated according to the
   * configuration. For more information, see the documentation.
   *
   * @type {ProxyConfiguration}
   * @memberof HomeDepotCrawlerOptions
   * @see https://docs.apify.com/proxy
   * @see https://console.apify.com/proxy
   */
  public proxyConfiguration?: ProxyConfiguration
  /**
   * Consumer-provided function that performs the logic of the crawler. It is
   * called for each URL to crawl.
   *
   * The function receives the `BasicCrawlingContext` as an argument, where the
   * `request` represents the URL to crawl.
   *
   * The function must return a `Promise`, which is then awaited by the crawler.
   *
   * If the function throws an exception, the crawler will try to re-crawl the
   * request later, up to the maxRequestRetries times. If all the retries fail,
   * the crawler calls the function provided to the `failedRequestHandler`
   * parameter. To make this work, we should always let our function throw
   * exceptions rather than catch them. The exceptions are logged to the
   * request using the `Request.pushErrorMessage()` function.
   *
   * @type {RequestHandler<CheerioCrawlingContext>}
   * @memberof HomeDepotCrawlerOptions
   */
  public requestHandler: RequestHandler<CheerioCrawlingContext> = homeDepotRouter
  /**
   * Timeout in which the function passed as `requestHandler` needs to finish,
   * in seconds.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default 30
   */
  public requestHandlerTimeoutSecs?: number = THIRTY
  /**
   * Static list of URLs to be processed. If not provided, the crawler will
   * open the default request queue when the `crawler.addRequests()` function
   * is called.
   *
   * ⓘ Alternatively, requests parameter of `crawler.run()` could be used to
   * enqueue the initial requests — it is a shortcut for running `crawler.
   * addRequests()` before the `crawler.run()`.
   *
   * @type {RequestList}
   * @memberof HomeDepotCrawlerOptions
   */
  public requestList?: RequestList
  /**
   * Dynamic queue of URLs to be processed. This is useful for recursive
   * crawling of websites. If not provided, the crawler will open the default
   * request queue when the` crawler.addRequests()` function is called.
   *
   * ⓘ Alternatively, requests parameter of crawler.run() could be used to
   * enqueue the initial requests - it is a shortcut for running `crawler.
   * addRequests()` before the `crawler.run()`.
   *
   * @type {RequestProvider}
   * @memberof HomeDepotCrawlerOptions
   */
  public requestQueue?: RequestProvider
  /**
   * If set to true, the crawler will automatically try to bypass any detected
   * bot protection.
   *
   * ⓘ Currently supports:
   *
   * - Cloudflare Bot Management
   * - Google Search Rate Limiting
   *
   * @type {boolean}
   * @memberof HomeDepotCrawlerOptions
   * @default false
   * @see https://www.cloudflare.com/products/bot-management/
   * @see https://www.google.com/sorry/
   */
  public retryOnBlocked?: boolean = false
  /**
   * Indicates how much time (in seconds) to wait before crawling another same
   * domain request.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   * @default 0
   */
  public sameDomainDelaySecs?: number = DELAY_SECONDS * MILLISECOND
  /**
   * The configuration options for `SessionPool` to use.
   *
   * @type {SessionPoolOptions}
   * @memberof HomeDepotCrawlerOptions
   * @see https://crawlee.dev/api/core/class/SessionPool
   */
  public sessionPoolOptions?: SessionPoolOptions
  /**
   * Customize the way statistics collecting works, such as logging interval or
   * whether to output them to the Key-Value store.
   *
   * @type {StatisticsOptions}
   * @memberof HomeDepotCrawlerOptions
   */
  public statisticsOptions?: StatisticsOptions
  /**
   * Allows overriding the default status message. The callback needs to call
   * `crawler.setStatusMessage()` explicitly. The default status message is
   * provided in the parameters.
   *
   * @type {StatusMessageCallback<
   *     BasicCrawlingContext,
   *     BasicCrawler
   *   >}
   * @memberof HomeDepotCrawlerOptions
   * @see https://crawlee.dev/api/cheerio-crawler/interface/CheerioCrawlerOptions#statusMessageCallback
   */
  public statusMessageCallback?: StatusMessageCallback<
  BasicCrawlingContext,
  BasicCrawler
  >

  /**
   * Defines the length of the interval for calling the setStatusMessage in
   * seconds.
   *
   * @type {number}
   * @memberof HomeDepotCrawlerOptions
   */
  public statusMessageLoggingInterval?: number
  /**
   * By default this crawler will extract correct encoding from the HTTP
   * response headers. Sadly, there are some websites which use invalid
   * headers. Those are encoded using the UTF-8 encoding. If those sites
   * actually use a different encoding, the response will be corrupted. You can
   * use `suggestResponseEncoding` to fall back to a certain encoding, if you
   * know that your target website uses it. To force a certain encoding,
   * disregarding the response headers, use `HttpCrawlerOptions.
   * forceResponseEncoding`.
   *
   * @type {string}
   * @memberof HomeDepotCrawlerOptions
   */
  public suggestResponseEncoding?: string
  /**
   * Basic crawler will initialize the SessionPool with the corresponding
   * `sessionPoolOptions`. The session instance will be than available in the
   * `requestHandler`.
   *
   * @type {boolean}
   * @memberof HomeDepotCrawlerOptions
   * @default true
   */
  public useSessionPool?: boolean = true

  constructor (options?: CheerioCrawlerOptions) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    Object.assign(this, options)
  }

  static load (options?: CheerioCrawlerOptions): HomeDepotCrawlerOptions {
    const crawlerOptions = new HomeDepotCrawlerOptions(options)
    return crawlerOptions
  }
}

const homeDepotCrawlerOptions = new HomeDepotCrawlerOptions()

export default homeDepotCrawlerOptions
export { HomeDepotCrawlerOptions }
