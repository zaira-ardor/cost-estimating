const handleCatchBlock = (error: unknown): Error => {
  if (error instanceof Error) {
    console.error(error.message)
    return error
  }
  const message = JSON.stringify(error)
  const err = new Error(message)
  console.error(err.message)
  return err
}

export default handleCatchBlock
