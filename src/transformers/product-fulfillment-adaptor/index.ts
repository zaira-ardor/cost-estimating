/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { type Product, type WithContext } from 'schema-dts'
import { type ProductClientOnlyProductAdaptee } from './interfaces/product-client-only-product-adaptee.d'
import setAvailableAtOrFrom from './internals/set-available-at-or-from'
import setAvailableDeliveryMethod from './internals/set-available-delivery-method'
import setDeliveryChargeSpecification from './internals/set-delivery-charge-specification'
import setIneligibleRegion from './internals/set-ineligible-region'
import setInventoryLevel from './internals/set-inventory-level'
const unset = require('lodash.unset')

class ProductFulfillmentAdaptor {
  public product: WithContext<Product>
  protected adaptee: ProductClientOnlyProductAdaptee

  constructor (product: WithContext<Product>, adaptee: object) {
    this.product = product
    this.adaptee = adaptee as ProductClientOnlyProductAdaptee
  }

  adapt (
    adaptee: ProductClientOnlyProductAdaptee = this.adaptee
  ): WithContext<Product> {
    this
      .removeReviews()
      .setAvailableAtOrFrom(adaptee)
      .setAvailableDeliveryMethod(adaptee)
      .setDeliveryChargeSpecification(adaptee)
      .setIneligibleRegion(adaptee)
      .setInventoryLevel(adaptee)
    return this.product
  }

  removeReviews (): this {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    unset(this.product, 'review') as boolean
    return this
  }

  setAvailableAtOrFrom (adaptee: ProductClientOnlyProductAdaptee = this.adaptee): this {
    setAvailableAtOrFrom(this.product, adaptee)
    return this
  }

  setAvailableDeliveryMethod (adaptee: ProductClientOnlyProductAdaptee = this.adaptee): this {
    setAvailableDeliveryMethod(this.product, adaptee)
    return this
  }

  setDeliveryChargeSpecification (adaptee: ProductClientOnlyProductAdaptee = this.adaptee): this {
    setDeliveryChargeSpecification(this.product, adaptee)
    return this
  }

  setIneligibleRegion (adaptee: ProductClientOnlyProductAdaptee = this.adaptee): this {
    setIneligibleRegion(this.product, adaptee)
    return this
  }

  setInventoryLevel (adaptee: ProductClientOnlyProductAdaptee = this.adaptee): this {
    setInventoryLevel(this.product, adaptee)
    return this
  }
}

export default ProductFulfillmentAdaptor
