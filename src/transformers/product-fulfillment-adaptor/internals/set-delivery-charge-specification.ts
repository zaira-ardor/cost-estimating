import { type Date, type DeliveryMethod, type DeliveryChargeSpecification, type Product } from 'schema-dts'
import { type ProductClientOnlyProductAdaptee, type Service } from '../interfaces/product-client-only-product-adaptee.d'
import deliveryMethodMap from './delivery-method-map'

const createPriceSpecification = (services: Service[], type: string): DeliveryChargeSpecification[] => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access
  return services
    .map(({ deliveryCharge, deliveryDates, locations, totalCharge }, index) => {
      const { startDate, endDate }: { startDate: Date, endDate: Date } = deliveryDates
      return {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        '@type': 'DeliveryChargeSpecification',
        areaServed: locations[index].state,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion, @typescript-eslint/no-unsafe-argument
        appliesToDeliveryMethod: deliveryMethodMap.get(type)! as DeliveryMethod,
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call
        price: (deliveryCharge ?? totalCharge) as number,
        priceCurrency: 'USD',
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
        validFrom: startDate,
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
        validThrough: endDate
      } satisfies DeliveryChargeSpecification
    })
}

function setDeliveryChargeSpecification (
  product: Product,
  adaptee: object
): Product {
  const obj = adaptee as ProductClientOnlyProductAdaptee
  const fulfillmentOptions = obj.data.product.fulfillment.fulfillmentOptions
  const priceSpecification = fulfillmentOptions
    .filter(({ fulfillable }) => fulfillable)
    .map(({ services, type }) => createPriceSpecification(services, type))
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  product.offers.priceSpecification = priceSpecification.flat()
  return product
}

export default setDeliveryChargeSpecification
export { createPriceSpecification }
