/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { type Product } from 'schema-dts'
import { type ProductClientOnlyProductAdaptee } from '../interfaces/product-client-only-product-adaptee.d'
import deliveryMethodMap from './delivery-method-map'

function setAvailableDeliveryMethod (
  product: Product,
  adaptee: object
): Product {
  const obj = adaptee as ProductClientOnlyProductAdaptee
  const fulfillmentOptions = obj.data.product.fulfillment.fulfillmentOptions
  const availableDeliveryMethod = fulfillmentOptions
    .filter(({ fulfillable }) => fulfillable)
    .map(({ type }) => deliveryMethodMap.get(type))
  // @ts-expect-error schema-dts throws false compilation errors
  product.offers.availableDeliveryMethod = availableDeliveryMethod
  return product
}

export default setAvailableDeliveryMethod
