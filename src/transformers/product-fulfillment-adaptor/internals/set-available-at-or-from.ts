/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { type Place, type Product } from 'schema-dts'
import { type Location, type ProductClientOnlyProductAdaptee } from '../interfaces/product-client-only-product-adaptee.d'

const createLocation = (location: Location): Place => ({
  '@type': 'HomeAndConstructionBusiness',
  address: {
    '@type': 'PostalAddress',
    addressLocality: 'Detroit',
    addressRegion: location.state
  },
  identifier: location.locationId,
  image: [],
  location: location.type,
  name: location.storeName ?? location.type
})

function setAvailableAtOrFrom (
  product: Product,
  adaptee: object
): Product {
  const obj = adaptee as ProductClientOnlyProductAdaptee
  const availableAtOrFrom =
    obj.data.product.fulfillment.fulfillmentOptions
      .filter(({ fulfillable }) => fulfillable)
      .map(({ services }) => services)
      .map(([service]) => service.locations)
      .map(([location]) => createLocation(location)) ?? undefined
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  product.offers.availableAtOrFrom = availableAtOrFrom
  return product
}

export default setAvailableAtOrFrom
