/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { type Product, type QuantitativeValue } from 'schema-dts'
import { type ProductClientOnlyProductAdaptee } from '../interfaces/product-client-only-product-adaptee.d'
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-var-requires
const has = require('lodash.has')

function setInventoryLevelQty (
  product: Product,
  quantity: number
): Product {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  product.offers.inventoryLevel = quantity as QuantitativeValue['value']
  return product
}

const INDEX = 0
const hasInventoryLevel = (adaptee: ProductClientOnlyProductAdaptee, index: number = INDEX): boolean => {
  const quantityPath = `data.product.fulfillment.fulfillmentOptions[${index}].services[${index}].locations[${index}].inventory.quantity`
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call
  return has(adaptee, quantityPath) as boolean
}

function setInventoryLevel (
  product: Product,
  adaptee: object
): Product {
  if (hasInventoryLevel(adaptee as ProductClientOnlyProductAdaptee)) {
    const obj = adaptee as ProductClientOnlyProductAdaptee
    const INDEX = 0
    const quantity: number = obj.data.product.fulfillment.fulfillmentOptions[INDEX]
      .services[INDEX]
      .locations[INDEX]
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      .inventory.quantity
    setInventoryLevelQty(product, quantity)
  }
  return product
}

export default setInventoryLevel
export { setInventoryLevelQty }
