import { type Product } from 'schema-dts'
import { type ProductClientOnlyProductAdaptee } from '../interfaces/product-client-only-product-adaptee.d'

function setIneligibleRegion (
  product: Product,
  adaptee: object
): Product {
  const obj = adaptee as ProductClientOnlyProductAdaptee
  const ineligibleRegion =
    obj.data.product.fulfillment.excludedShipStates.split(/,/g)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  product.offers.ineligibleRegion = ineligibleRegion
  return product
}

export default setIneligibleRegion
