const deliveryMethodMap = new Map([
  ['delivery', 'DeliveryMethod.ParcelService'],
  ['pickup', 'DeliveryMethod.OnSitePickup']
])

export default deliveryMethodMap
