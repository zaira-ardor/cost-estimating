/* c8 ignore start */
export interface ProductClientOnlyProductAdaptee {
  data: Data
}

export interface Data {
  product: Product
}

export interface Product {
  __typename: string
  availabilityType: AvailabilityType
  badges: Badge[]
  dataSource: string
  dataSources: string
  details: Details
  favoriteDetail: FavoriteDetail
  fulfillment: Fulfillment
  identifiers: Identifiers
  info: Info
  itemId: string
  keyProductFeatures: unknown
  media: Media
  pricing: Pricing
  reviews: Reviews
  seo: Seo
  seoDescription: string
  sizeAndFitDetail: SizeAndFitDetail
  specificationGroup: SpecificationGroup[]
  subscription: unknown
  taxonomy: Taxonomy
}

export interface AvailabilityType {
  __typename: string
  buyable: boolean
  discontinued: boolean
  status: boolean
  type: string
}

export interface Badge {
  __typename: string
  color: unknown
  creativeImageUrl: unknown
  endDate: unknown
  label: string
  message: string
  name: string
  timer: unknown
  timerDuration: string
}

export interface Details {
  __typename: string
  collection: unknown
  description: string
  highlights: string[]
  installation: unknown
}

export interface FavoriteDetail {
  __typename: string
  count: number
}

export interface Fulfillment {
  __typename: string
  anchorStoreStatus: boolean
  anchorStoreStatusType: string
  backordered: boolean
  backorderedShipDate: unknown
  bodfsAssemblyEligible: boolean
  bossExcludedShipState: unknown
  bossExcludedShipStates: string
  excludedShipStates: string
  fallbackMode: boolean
  fulfillmentOptions: FulfillmentOption[]
  inStoreAssemblyEligible: boolean
  onlineStoreStatus: boolean
  onlineStoreStatusType: string
  seasonStatusEligible: unknown
  sthExcludedShipState: unknown
}

export interface FulfillmentOption {
  __typename: string
  fulfillable: boolean
  services: Service[]
  type: string
}

export interface Service {
  __typename: string
  deliveryCharge: unknown
  deliveryDates: DeliveryDates
  deliveryTimeline?: string
  dynamicEta: unknown
  freeDeliveryThreshold: unknown
  hasFreeShipping: boolean
  hasSameDayCarDelivery: boolean
  locations: Location[]
  optimalFulfillment: boolean
  totalCharge: number
  type: string
}

export interface DeliveryDates {
  __typename: string
  endDate: string
  startDate: string
}

export interface Location {
  __typename: string
  curbsidePickupFlag?: boolean
  distance: unknown
  inventory: Inventory
  isAnchor: boolean
  isBuyInStoreCheckNearBy: unknown
  locationId: string
  state: string
  storeName?: string
  storePhone: unknown
  storeTimeZone: unknown
  type: string
}

export interface Inventory {
  __typename: string
  isInStock: boolean
  isLimitedQuantity: boolean
  isOutOfStock: boolean
  isUnavailable: boolean
  maxAllowedBopisQty: unknown
  minAllowedBopisQty: unknown
  quantity: number
}

export interface Identifiers {
  __typename: string
  brandName: string
  canonicalUrl: string
  isSuperSku: boolean
  itemId: string
  modelNumber: string
  parentId: unknown
  productLabel: string
  productType: string
  rentalCategory: unknown
  rentalSubCategory: unknown
  roomVOEnabled: boolean
  sampleId: unknown
  skuClassification: string
  specialOrderSku: unknown
  storeSkuNumber: string
  toolRentalSkuNumber: unknown
  upc: string
  upcGtin13: string
}

export interface Info {
  __typename: string
  augmentedReality: boolean
  bathRenovation: boolean
  categoryHierarchy: unknown
  classNumber: string
  consultationType: string
  customerSignal: unknown
  dotComColorEligible: boolean
  ecoRebate: boolean
  eligibleProtectionPlanSkus: unknown
  fiscalYear: unknown
  forProfessionalUseOnly: boolean
  gccExperienceOmsId: unknown
  globalCustomConfigurator: unknown
  hasServiceAddOns: boolean
  hasSubscription: boolean
  hidePrice: boolean
  isBuryProduct: unknown
  isGenericProduct: unknown
  isInStoreReturnMessageEligible: boolean
  isLiveGoodsProduct: boolean
  isSponsored: unknown
  label: unknown
  minimumOrderQuantity: number
  movingCalculatorEligible: boolean
  paintBrand: unknown
  paintFamily: unknown
  pipCalculator: unknown
  productDepartment: string
  productDepartmentId: unknown
  productSubType: unknown
  projectCalculatorEligible: boolean
  protectionPlanSku: unknown
  quantityLimit: number
  recommendationFlags: RecommendationFlags
  replacementOMSID: unknown
  returnable: string
  samplesAvailable: boolean
  sponsoredBeacon: unknown
  sponsoredMetadata: unknown
  sskMax: unknown
  sskMin: unknown
  subClassNumber: string
  swatches: unknown
  totalNumberOfOptions: unknown
  unitOfMeasureCoverage: unknown
  wasMaxPriceRange: unknown
  wasMinPriceRange: unknown
}

export interface RecommendationFlags {
  __typename: string
  ACC: boolean
  batItems: boolean
  bundles: boolean
  collections: boolean
  frequentlyBoughtTogether: boolean
  packages: boolean
  pipCollections: boolean
  visualNavigation: boolean
}

export interface Media {
  __typename: string
  augmentedRealityLink: unknown
  images: Image[]
  threeSixty: unknown
  video: unknown[]
}

export interface Image {
  __typename: string
  altText: unknown
  sizes: string[]
  subType?: string
  type: string
  url: string
}

export interface Pricing {
  __typename: string
  alternate: Alternate
  alternatePriceDisplay: boolean
  conditionalPromotions: unknown[]
  mapAboveOriginalPrice: unknown
  message: unknown
  original: unknown
  preferredPriceFlag: boolean
  promotion: unknown
  specialBuy: unknown
  unitOfMeasure: string
  value: number
}

export interface Alternate {
  __typename: string
  bulk: unknown
  unit: Unit
}

export interface Unit {
  __typename: string
  caseUnitOfMeasure: unknown
  unitsOriginalPrice: unknown
  unitsPerCase: unknown
  value: unknown
}

export interface Reviews {
  __typename: string
  ratingsReviews: RatingsReviews
}

export interface RatingsReviews {
  __typename: string
  averageRating: string
  totalReviews: string
}

export interface Seo {
  __typename: string
  seoDescription: string
  seoKeywords: unknown
}

export interface SizeAndFitDetail {
  __typename: string
  attributeGroups: unknown[]
}

export interface SpecificationGroup {
  __typename: string
  specifications: Specification[]
  specTitle: string
}

export interface Specification {
  __typename: string
  specName: string
  specValue: string
}

export interface Taxonomy {
  __typename: string
  brandLinkUrl: string
  breadCrumbs: BreadCrumb[]
}

export interface BreadCrumb {
  __typename: string
  browseUrl: unknown
  creativeIconUrl: unknown
  deselectUrl: unknown
  dimensionName: string
  label: string
  refinementKey: unknown
  url: string
}
/* c8 ignore end */
