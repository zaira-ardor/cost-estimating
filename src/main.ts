import HomeDepotProductExtractor, {
  homeDepotCrawlerOptions
} from './crawlers/hd'

homeDepotCrawlerOptions.maxRequestsPerCrawl = 20
const extractor = new HomeDepotProductExtractor(homeDepotCrawlerOptions)
await extractor.run([
  'https://www.homedepot.com/sitemap/P/PIPs/21/021-001-0.xml'
])
