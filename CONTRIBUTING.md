# Contributing guide

> <dfn>Contributions</dfn> are changes to (software) _products_, _projects_, and _communities_.

## Overview

A contribution has a beginning, a middle, and an end. Contributions:

1. Begin with **Issues**,
2. Occur in **Merge Requests**, and
3. End with **Merges** into the `main` product branch.

We use Gitlab to organize how we carry out our:

1. **Communication** workflow and
2. **Product** _deployment_ or _delivery_ workflow

## Table of contents

<!-- toc -->

+ [1. **Issues**](#1-issues)
+ [2. **Git**](#2-git)
+ [3. **Code standards**](#3-code-standards)
+ [4. **Unit testing**](#4-unit-testing)
+ [5. **Directory structure**](#5-directory-structure)
+ [6. **Logging**](#6-logging)
+ [7. **Dependencies**](#7-dependencies)
+ [8. **APIs**](#8-apis)
  + [8.1. Command-line interfaces](#81-command-line-interfaces)
  + [8.2. Web services](#82-web-services)
+ [9. **Licensing**](#9-licensing)

<!-- tocstop -->

## 1. **Issues**

![Issues][gl-icon-issues]{width="100px" height="100px"}

+ **Collaboration starts with _Issues_. Changes happen through _Merge Requests_.**

  View `zaira-ardor-llc/architecture/decision-records's` collaboration and contribution flowcharts:

  ***

  <details>
  <summary><img src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issues.svg?ref_type=heads" height="24" alt="issues"> Toggle view of the Issue workflow flowchart.

  </summary>

  ![Issue flowchart](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/contribution-lifecycle-create-issue.png)

  </details>

  ***

  <details>
  <summary><img src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request.svg?ref_type=heads" height="24" alt="issues"> Toggle view of the merge request workflow flowchart.

  </summary>

  ![merge request flowchart](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/contribution-lifecycle-MR.png)

  </details>

  ***

### 1.1. **Create Issues for feature requests and defects**

@zaira-ardor-llc follows an issue-driven product delivery model. Before any work is done, create an Issue, first. This starts a conversation about features, defects ("bugs"), refactoring, product delivery improvements, etc.

### 1.2. Format titles with **`<type>(<scope>): <subject>`**

+ _Why:_

  > ⁍`type` categorizes product changes. Valid types are:
  >
  > + `build`: Changes that affect the build system or external dependencies.
  > + `ci`: Changes related to continuous integration, delivery, and deployment tasks.
  > + `docs`: Documentation changes.
  > + `feat`: A new feature.
  > + `fix`: Defect (bug) repair.
  > + `perf`: Performance enhancements.
  > + `refactor`: Source code design improvements that don't affect product behavior.
  > + `revert`: Go back to a previous release verison.
  > + `style`: Changes involving graphics, typography, etc., as well as source code beautification.
  > + `test`: Tests added to increase code coverage, or corrected due to errors.

### 1.3. Fill out the issue template

+ _Why:_

  > ⁍ It keeps communication consistent and unambiguous.

### 1.4. Label the issue (optional)

+ _Why:_

  > ⁍ We use [`git-labelmaker`](https://github.com/himynameisdave/git-labelmaker) to categorize Issues (and merge Requests) consistently. There are four label categories:
  >
  > + `type`: the "kind" of product change.
  > + `work`: the current state of a work-item; the status of a change.
  > + `urgency`: the importance and value of a change.

  ***

  <details>
  <summary>

  ![Help](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-help-48.png){width="48" height="48"} Toggle view of the Label definitions table.

  </summary>

  <table>
  <tr>
  <th>

  Label :label:
  </th>
  <th>Definition</th>
  </tr>
  <tr>
  <td>

  `Type: Feature`
  </td>
  <td>

  <dfn>A distinguished or expected characteristic of a product that either differentiates the product from competitors, or whose absence would be diminish the product’s value.</dfn><br><br>**Note** that `Type: Feature` and `Type: Defect` are mutually exclusive: an Issue cannot be both a feature and a defect.
  </td>
  </tr>
  <tr>
  <td>

  `Type: Defect`
  </td>
  <td>

  <dfn>A flaw, fault, or abnormality that deviates from or prevents the product’s expected behavior.</dfn><br><br>**Note** that `Type: Feature` and `Type: Defect` are mutually exclusive: an Issue cannot be both a feature and a defect.
  </td>
  </tr>
  <tr>
  <td>

  `CLA: Signed`
  </td>
  <td>

  <dfn>The person who submitted a product change has signed your Contributor License Agreement.</dfn><br><br>Remove this label if your product owner does not require a CLA.
  </td>
  </tr>
  <tr>
  <td>

  `CLA: Unsigned`
  </td>
  <td>

  <dfn>The person who submitted a product change has \*\*_not_\*\*signed your Contributor License Agreement.</dfn><br><br>Remove this label if your product owner does not require a CLA.
  </td>
  </tr>
  <tr>
  <td>

  `Priority: Critical`
  </td>
  <td>

  `Type: Feature`: <dfn>The proposed enhancement is essential to the success of your product.</dfn><br><br>`Type: Defect`: <dfn>Your product no longer functions due to internal, `FATAL` errors, and must be addressed immediately in order to maintain consumer loyalty.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Priority: High`
  </td>
  <td>

  `Type: Feature`: <dfn>The proposed enhancement is central to product’s value proposition, and should be implemented as soon as possible.</dfn><br><br>`Type: Defect`: <dfn>The product functions overall, but with an issue that risks consumer abandonment.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Priority: Medium`
  </td>
  <td>

  `Type: Feature` or `Type: Defect`: <dfn>The proposed change should be implemented as long as no `Priority: Critical` or `Priority: High` issues exists.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Priority: Low`
  </td>
  <td>

  `Type: Feature`: <dfn>A proposal that minimally affects the product’s value.</dfn><br><br>`Type: Defect`: <dfn>Represents “cosmetic” problems like misspelled words or misaligned text that do not affect branding and marketing strategy.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: Abandoned`
  </td>
  <td>

  `Type: Feature` or `Type: Defect`: <dfn>The team and community have neglected, forgotten, discarded, or ignored resolving a Issue.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: Accepted`
  </td>
  <td>

  `Type: Feature` or `Type: Defect`: <dfn>The product owner or maintainers agreed to a product change proposal.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: Available`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The change proposal is ready for team and community members to work on.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: Blocked`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The proposed change cannot be addressed until another issue has been resolved.</dfn><br><br>**Note** that the Issue blocking the proposed change SHOULD be referenced in the `Blocked` Issue’s description field.
  </td>
  </tr>
  <tr>
  <td>

  `Status: Completed`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The issue has been resolved and all acceptance criteria validated.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: In Progress`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The team or community is actively working on the Issue’s resolution.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: On Hold`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The Product Owner has (temporarily) postponed Issue resolution.</dfn><br><br>**Note** that the _reason_ for postponement should be stated in the Issue’s description field.
  </td>
  </tr>
  <tr>
  <td>

  `Status: Pending`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>product change or resolution is either awaiting the Product Owner’s decision.</dfn> Ideally, the Product Owner should declare why they’re undecided somewhere in the Issue thread.
  </td>
  </tr>
  <tr>
  <td>

  `Status: Rejected`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The Product Owner has declined a change proposal.</dfn><br><br>**Note** that the Product Owner should politely explain why they dismissed the change request.
  </td>
  </tr>
  <tr>
  <td>

  `Status: Review Needed`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The person working on an Issue has requested help or discussion.</dfn> When applied to a merge request, `Status: Review Needed` <dfn>signifies that the MR is ready for evaluation (and potentially, approval).</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Status: Revision Needed`
  </td>
  <td>

  `Type: Feature` and `Type: Defect`: <dfn>The Issue is not ready for evaluation because of incomplete or insufficient information.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Breaking Change`
  </td>
  <td>

  <dfn>The change introduces backward incompatibility with previous product versions.</dfn><br><br>**`Type: Breaking Change` MUST be recorded with a**

  1.
  2. Git commit message,
  3. An increment (+1) in the product’s Semantic Version’s MAJOR version,
  4. `CHANGELOG` entry, and
  5. Updated API documentation.
  </td>
  </tr>
  <tr>
  <td>

  `Type: Build`
  </td>
  <td>

  <dfn>Changes to the process that convert source code into a stand-alone form that can be run on a computer or to the form itself.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Chore`
  </td>
  <td>

  <dfn>Miscellaneous non-functional changes such as typographical fixes or source code repository initialization</dfn>, e.g., `chore(scm): scaffold product directory structure`
  </td>
  </tr>
  <tr>
  <td>

  `Type: CI`
  </td>
  <td>

  <dfn>Continuous Integration (CI) changes, i.e., automated build, test, an quality assurance tasks.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type:https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs`
  </td>
  <td>

  <dfn>The introduction of or revisions to natural language documents or source code comments.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Duplicate`
  </td>
  <td>

  <dfn>An Issue that shares the same characteristics as a previously reported issue.</dfn> <br><br>**Note** that product maintainers should reference the original Issue and close the `Type: Duplicate` Issue.
  </td>
  </tr>
  <tr>
  <td>

  `Type: Feedback`
  </td>
  <td>

  <dfn>A response to a `Type: Question` or voluntary information used as a basis for improvement.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Fix`
  </td>
  <td>

  <dfn>A change intended to repair a `Type: Defect` Issue.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Performance`
  </td>
  <td>

  <dfn>A change intended to reduce product latency.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Question`
  </td>
  <td>

  <dfn>A request for information.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Refactor`
  </td>
  <td>

  <dfn>Source code design improvements that do not affect product behavior.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Revert`
  </td>
  <td>

  <dfn>Changes that return the product’s source code to previous Git commit hash.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Spike`
  </td>
  <td>

  <dfn>A technical or design _experiment_ that investigates a possible solution.</dfn> <br><br>**Note** that spike solutions are, by definition, _throwaway_ solutions that should **NEVER** be added to a product release.
  </td>
  </tr>
  <tr>
  <td>

  `Type: Style`
  </td>
  <td>

  <dfn>Issues that address code standard _or_ brand compliance.</dfn>
  </td>
  </tr>
  <tr>
  <td>

  `Type: Test`
  </td>
  <td>

  <dfn>Issues that prove intended behavior or substantiate “definitions of done.”</dfn> <br><br>`Type: Test` can also refer to changes that result in broader code coverage.
  </td>
  </tr>
  </table>

  </details>

  ***

+ 1.5. Monitor your issue for questions

  _Why:_

  > ⁍ The team might need more clarification.

+ 1.6. Your issue will be either accepted for work, or declined

  _Why:_

  > ⁍ It's up to the Product Owner to agree to proposed changes. If they believe your issue add value, the issue will be approved, and we'll ask someone to volunteer to do the work.
  >
  > Otherwise, your issue will be politely declined.

## 2. **Git**

![Git Logo](https://img.icons8.com/?size=100&id=20906&format=png)

+ 2.1. **Rules**

  `zaira-ardor-llc/architecture/decision-records` manages contributions with the [feature-branch-workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow).

+ 2.1.1. Makes changes in a topic branch

  _Why:_

  > ⁍ Use an isolated topic branch for parallel product development. Topic branches allow you to submit multiple merge requests without confusion. You can iterate without polluting the main branch with potentially unstable, unfinished code. The `zaira-ardor-llc/architecture/decision-records` team uses:
  >
  > + [Feature-branch-workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow) for small-ish codebases, or
  > + [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) for large applications and monoliths

+ 2.1.2. Favor the topic branch naming recommendation `type/issue-change-name`

  _Why:_

  > ⁍ Although not required, our team prefixes branches with the type of change being introduced, followed by a forward slash and the issue id.
  >
  > Pattern: `type/issueId-subject`\
  > Icon legend: ![Bitbucket branch prefix](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-bitbucket-20.png) Bitbucket ![Gitlab branch prefix](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Gitlab
  >
  > <dl>
  > <dt>
  >
  > </dt>
  > <dt>
  >
  > `bugfix/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Bitbucket branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-bitbucket-20.png) Defect (bug) repair issues.
  >
  > </dd>
  > <dt>
  >
  > `build/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Issues related to product builds.
  >
  > </dd>
  > <dt>
  >
  > `ci/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Issues related to continuous integration, delivery, and deployment tasks.
  >
  > </dd>
  > <dt>
  >
  > `docs/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Issues related to documentation.
  >
  > </dd>
  > <dt>
  >
  > `feat/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) New feature or enhancement requests.
  >
  > </dd>
  > <dt>
  >
  > `feature/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Bitbucket branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-bitbucket-20.png) New feature or enhancement requests.
  >
  > </dd>
  > <dt>
  >
  > `fix/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Defect (bug) repair issues.
  >
  > </dd>
  > <dt>
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Bitbucket branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-bitbucket-20.png) \`hotfix/\`
  >
  > </dd>
  > <dt>
  >
  > `perf/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Performance improvement issues.
  >
  > </dd>
  > <dt>
  >
  > `refactor/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Source code design \*\*improvements that do not affect product behavior\*\*.
  >
  > </dd>
  > <dt>
  >
  > `revert/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Tasks that revert to a previous commit hash.
  >
  > </dd>
  > <dt>
  >
  > `spike/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Issues related in solution investigation.
  >
  > </dd>
  > <dt>
  >
  > `style/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Issues related to style guideline compliance, especially \`ESLint\` errors and warnings.
  >
  > </dd>
  > <dt>
  >
  > `test/`
  >
  > </dt>
  > <dd>
  >
  > ![Applies to Gitlab branches](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/icons8/icon-github-filled-20.png) Test coverage tasks.
  >
  > </dd>
  > </dl>

+ 2.1.3. Branch out from `main`

  _Why:_

  > ⁍ `zaira-ardor-llc/architecture/decision-records` follows the feature-branch-workflow.

+ 2.1.4. **_Never_** push into the `main` branch. **_Always_** submit a merge request

  _Why:_

  > ⁍ It notifies team members whenever changes occur and allows the community to review your changes at any time..
  >
  > It also enables easy peer-review of the code and dedicates forum for discussing the proposed feature.

+ 2.1.5. Submit a merge request as soon as possible

  _Why:_

  > ⁍ merge Requests declare work in progress. Frequent pushes to a merge request notify your team members about change, and gives them the opportunity to provide feedback more often.
  >
  > merge request pushes also trigger automated CI-services, which help you fail fast and assess quality.

+ 2.1.6. Rebase your local `main` branch before you ask for MR approvals

  _Why:_

  > ⁍ Rebasing will merge in the requested branch (`main` or `topic`) and apply the commits that you have made locally to the top of the history without creating a merge commit (assuming there were no conflicts). This results in a nice and clean history.

+ 2.1.7. Resolve rebase conflicts before merge request reviews

  _Why:_

  > ⁍ Rebasing will merge in the `main` branch and apply the commits that you have made locally to the top of it.

+ 2.1.8. Add reviewers and the label `Status: Needs Review` when the topic branch is ready

  _Why:_

  > ⁍ When you add a Reviewer, Gitlab (or Bitbucket) notifies teammates that your topic branch meets all Acceptance Criteria and is ready to be merged into `main`.
  >
  > Add the label "Status: Review Needed" formally declares the status of your topic branch, and helps teams filter through issues.

+ 2.1.9. Delete local and remote topic branches after merging

  _Why:_

  > ⁍ Topic branches should only exist while work is in-progress. Merged topic branches clutter up your list of branches with dead branches. Topic branch deletion also insures that you only ever merge back into `main`.

+ 2.1.10. Protect your `main` branch

  _Why:_

  > ⁍ Branch protection prevents production-ready branches from incorporating unexpected and irreversible changes. Learn more about
  >
  > + [Gitlab protected branches](https://help.github.com/articles/about-protected-branches/) and
  > + [Bitbucket protected branches](https://confluence.atlassian.com/bitbucketserver/using-branch-permissions-776639807.html).

+ 2.2. **Feature-branch-workflow**

  We use the [feature-branch-workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow). We _recommend_ [interactive rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing#the-golden-rule-of-rebasing), too, but that's not required.

+ 2.2.1. Initialize a Git repository in the product directory (_for new repositories only_)

  For subsequent features and changes, this step should be ignored.

  ```sh
  cd <product-repo-directory>
  git init
  ```

+ 2.2.2. Checkout a new `feat`ure or `fix` branch

  ```sh
  # For a new feature branch:
  git checkout -b feat/<issueId>-scope-of-change

  # For branches that address defects:
  git checkout -b fix/<issueId>-scope-of-change
  ```

+ 2.2.3. Make Changes

  ```sh
  git add
  git commit -a
  ```

  _Why:_

  > ⁍ `git commit -a` will start an editor which lets you separate the subject from the body. Read more about it in _section 1.3_.

+ 2.2.4. Follow the Conventional Commits Specification for commit messages

  This project enforces [AngularJS Git Commit Guidelines](https://github.com/angular/angular.js/blob/main/CONTRIBUTING.md#commit) (which is now an extension of the [Conventional Commits Specfication](https://conventionalcommits.org)) with [`commitplease`](https://www.npmjs.com/package/commitplease) pre-commit hooks.

  _Why:_

  > Consistent, legible Git logs not only facilitate communication, but also enable automated `CHANGELOG` generation and semantic versioning with [`standard-version`](https://github.com/conventional-changelog/standard-version).

  + **`build` commit messages**

    Issues related to product builds.

    ```
    build(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`chore` commit messages**

    Issues related to miscellaneous non-functional changes (usually "maintenance" changes).

    ```
    chore(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`ci` commit messages**

    Issues related to continuous integration, delivery, and deployment tasks.

    ```
    ci(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`docs` commit messages**

    Issues related to documentation.

    ```git
    docs(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`feat` (feature) commit messages**

    New feature or enhancement requests.

    ```
    feat(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`fix` commit messages**

    Defect (bug) repair issues.

    ```
    fix(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`perf` (performance) commit messages**

    Performance improvement issues.

    ```
    perf(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`refactor` commit messages**

    Source code design **improvements that do not affect product behavior**.

    ```
    refactor(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`revert` commit messages**

    Tasks that revert to a previous commit hash. Your message should begin with `revert:`, followed by the header of the reverted commit.

    In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

    ```
    revert: <hash>
    <BLANK LINE>
    This reverts commit <hash>.
    <BLANK LINE>
    <footer>
    ```

  + **`style` commit messages**

    Issues related to style guideline compliance, especially `ESLint` errors and warnings.

    ```
    style(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

  + **`test` commit messages**

    Test coverage tasks.

    ```
    test(<scope>): <subject>
    <BLANK LINE>
    <[body]>
    <BLANK LINE>
    <footer>
    ```

+ 2.2.5. Sync with remote to get changes you’ve missed

  ```sh
  git checkout main
  git merge
  ```

  _Why:_

  > ⁍ This will give you a chance to deal with conflicts on your machine while rebasing(later) rather than creating a merge request that contains conflicts.

+ 2.2.6. Update your topic branch with the latest changes from `main` by interactive rebase

  ```sh
  git checkout <branchname>
  git rebase -i --autosquash main
  ```

  _Why:_

  > ⁍ You can use `--autosquash` to squash all your commits to a single commit. Nobody wants many commits for a single feature in develop branch.
  >
  > [Learn more about autosquashing Git commits](https://robots.thoughtbot.com/autosquashing-git-commits).

+ 2.2.7. Resolve conflicts (if any occur), and continue rebase

  ```sh
  git add <file1> <file2> ...
  git rebase --continue
  ```

  [Learn more about resolving conflicts](https://help.github.com/articles/resolving-a-merge-conflict-using-the-command-line/).

+ 2.2.8. Push your branch with the `-f` flag (if necessary)

  Rebase changes history, so you might need to force changes into the `remote` branch with the `-f` flag. If someone else is working on your branch, use the less destructive `--force-with-lease`.

  ```sh
  git push -f
  ```

  _Why:_

  > ⁍ When you do a rebase, you are changing the history on your topic branch. As a result, Git will reject normal `git push`. Instead, you'll need to use the -f or --force flag.
  >
  > [Learn more about `--force-with-lease`](https://developer.atlassian.com/blog/2015/04/force-with-lease/).

+ 2.2.9. Submit a merge request
+ 2.2.10. Once accepted, the merge request will be merged, closed, and deleted by an administrator
+ 2.2.11. Remove your local topic branch if you're done

  ```sh
  git branch -d <branchname>
  ```

  to remove all branches which are no longer on remote

  ```sh
  git fetch -p && \
    for branch in `git branch -vv | grep ': gone]' | awk '{print $1}'`; \
      do git branch -D $branch; \
    done
  ```

## 3. **Code standards**

[![JavaScript Style Guide](https://cdn.jsdelivr.net/gh/standard/standard/badge.svg)](https://github.com/standard/standard) [![ESLint logo](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/logo-eslint.png)](https://eslint.org)

+ 3.1. Use the Standard JS Style

  `zaira-ardor-llc/architecture/decision-records` follows the [Standard JS Style](https://github.com/standard/standard).

+ 3.2. Use ESLint to analyze source code

  _Why:_

  > ⁍ [ESLint](https://eslint.org) evaluates JavaScript code (and `--fix`es what it can) whenever `npm test` runs. You can run ESLint directly with:

  ```shell
  npm run lint:js
  ```

  >

  View zaira-ardor-llc/architecture/decision-records's ESLint rules and their enforcement.

## 4. **Unit testing**

[![Jest JavaScript Testing](https://gitlab.com/commonalities/home-depot-product-extractor/-/raw/1-ci-scm-init-repo/docs/img/logo-jest.png)](https://facebook.github.io/jest/)

+ 4.1. Write Jest tests

  _Why:_

  > ⁍ Behavior-driven development specifications are executable documentation.

  + **Put test files in the \*_test\*_ directory.**
  + **Use the `.spec.js` suffix for all tests.**

+ 4.2. Reach 100% code coverage

  _Why:_

  > ⁍ Full coverage makes automated dependency drift updates safer.

  + View a test coverage summary in the Terminal:

    ```shell
    npm test

    > jest --config=jest.config.json

      PASS  **tests**/app.js
       zaira-ardor-llc/architecture/decision-records:app
        ✓ creates files (1ms)

    Test Suites: 1 passed, 1 total
    Tests:       1 passed, 1 total
    Snapshots:   0 total
    Time:        2.595s
    Ran all test suites.
    ----------|----------|----------|----------|----------|----------------|
    File      |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
    ----------|----------|----------|----------|----------|----------------|
    All files |      100 |      100 |      100 |      100 |                |
     index.js |      100 |      100 |      100 |      100 |                |
    ----------|----------|----------|----------|----------|----------------|
    ```

  + Open `/coverage/lcov-report/index.html` in a Web browser to view detailed coverage reports.

## 5. **Directory structure**

## 6. **Logging**

## 7. **Dependencies**

[![StackShare](https://img.shields.io/badge/tech-stack-0690fa.svg?style=flat-square)](https://stackshare.io/commonality/zaira-ardor-llc/architecture/decision-records)

`zaira-ardor-llc/architecture/decision-records` requires the following tech stack to either run, build, test, or deploy:

| **Dependency**                                                                                               | **Description**                                                                                                                                 | **Version** | **Type**   |
| ------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------- | ----------- | ---------- |
| [generator-license@5.1.0](https://github.com/jozefizso/generator-license)                                    | License generator for Yeoman based projects.                                                                                                    | 5.1.0       | production |
| [generator-node@2.2.0](https://github.com/yeoman/generator-node)                                             | Create a Node.js module                                                                                                                         | 2.2.0       | production |
| [git-remote-origin-url@2.0.0](https://github.com/sindresorhus/git-remote-origin-url#readme)                  | Get the remote origin url of a git repository                                                                                                   | 2.0.0       | production |
| [git-url-parse@7.0.1](https://github.com/IonicaBizau/git-url-parse)                                          | A high level git url parser for common git providers.                                                                                           | 7.0.1       | production |
| [github-username@4.1.0](https://github.com/sindresorhus/github-username#readme)                              | Get a Gitlab username from an email address                                                                                                     | 4.1.0       | production |
| [inquirer-npm-name@2.0.0](https://github.com/SBoudrias/inquirer-npm-name#readme)                             | Helper function using inquirer to validate a value provided in a prompt does not exist as an npm package.                                       | 2.0.0       | production |
| [lodash@^4.17.4](https://lodash.com/)                                                                        | Lodash modular utilities.                                                                                                                       | 4.17.4      | production |
| [markdown-magic@0.1.18](https://npmjs.org/package/markdown-magic)                                            | Automatically update markdown files with content from external sources                                                                          | 0.1.18      | production |
| [markdown-magic-install-command@1.3.1](https://github.com/camacho/markdown-magic-install-command#readme)     | Print install command for markdown file                                                                                                         | 1.3.1       | production |
| [markdown-magic-package-scripts@1.2.0](https://github.com/camacho/markdown-magic-package-scripts#readme)     | Print list of scripts in package.json with descriptions                                                                                         | 1.2.0       | production |
| [parse-author@2.0.0](https://github.com/jonschlinkert/parse-author)                                          | Parse an author, contributor, maintainer or other 'person' string into an object with name, email and url properties following npm conventions. | 2.0.0       | production |
| [update-notifier@2.2.0](https://github.com/yeoman/update-notifier#readme)                                    | Update notifications for your CLI app                                                                                                           | 2.2.0       | production |
| [yeoman-generator@2.0.0](http://yeoman.io)                                                                   | Rails-inspired generator system that provides scaffolding for your apps                                                                         | 2.0.0       | production |
| [babel-jest@21.0.2](https://github.com/facebook/jest#readme)                                                 | Jest plugin to use babel for transformation.                                                                                                    | 21.0.2      | dev        |
| [babel-preset-env@1.6.0](https://babeljs.io/)                                                                | A Babel preset for each environment.                                                                                                            | 1.6.0       | dev        |
| [babelify@7.3.0](https://github.com/babel/babelify)                                                          | Babel browserify transform                                                                                                                      | 7.3.0       | dev        |
| [check-node-version@2.1.0](https://github.com/parshap/check-node-version#readme)                             | Check installed versions of node and npm                                                                                                        | 2.1.0       | dev        |
| [commitplease@2.7.10](https://github.com/jzaefferer/commitplease#readme)                                     | Validates strings as commit messages                                                                                                            | 2.7.10      | dev        |
| [coveralls@^2.13.1](https://github.com/nickmerwin/node-coveralls#readme)                                     | takes json-cov output into stdin and POSTs to coveralls.io                                                                                      | 2.13.1      | dev        |
| [eslint@4.7.2](http://eslint.org)                                                                            | An AST-based pattern checker for JavaScript.                                                                                                    | 4.7.2       | dev        |
| [eslint-config-xo-space@0.16.0](https://github.com/sindresorhus/eslint-config-xo-space#readme)               | ESLint shareable config for XO with 2-space indent                                                                                              | 0.16.0      | dev        |
| [eslint-plugin-import@2.7.0](https://github.com/benmosher/eslint-plugin-import)                              | Import with sanity.                                                                                                                             | 2.7.0       | dev        |
| [eslint-plugin-jest@21.1.0](https://github.com/facebook/jest#readme)                                         | Eslint rules for Jest                                                                                                                           | 21.1.0      | dev        |
| [eslint-plugin-jsdoc@3.1.3](https://github.com/gajus/eslint-plugin-jsdoc#readme)                             | JSDoc linting rules for ESLint.                                                                                                                 | 3.1.3       | dev        |
| [eslint-plugin-no-unsafe-innerhtml@1.0.16](https://github.com/mozfreddyb/eslint-plugin-no-unsafe-innerhtml/) | custom ESLint rule to disallows unsafe innerHTML, outerHTML and insertAdjacentHTML                                                              | 1.0.16      | dev        |
| [eslint-plugin-no-unsanitized@2.0.1](https://github.com/mozilla/eslint-plugin-no-unsanitized/)               | ESLint rule to disallow unsanitized code                                                                                                        | 2.0.1       | dev        |
| [eslint-plugin-node@5.1.1](https://github.com/mysticatea/eslint-plugin-node#readme)                          | Additional ESLint's rules for Node.js                                                                                                           | 5.1.1       | dev        |
| [eslint-plugin-promise@3.5.0](https://github.com/xjamundx/eslint-plugin-promise#readme)                      | Enforce best practices for JavaScript promises                                                                                                  | 3.5.0       | dev        |
| [eslint-plugin-scanjs-rules@0.2.1](https://github.com/mozfreddyb/eslint-plugin-scanjs-rules/)                | ESLint plugin that contains ScanJS rules                                                                                                        | 0.2.1       | dev        |
| [eslint-plugin-security@1.4.0](https://github.com/nodesecurity/eslint-plugin-security#readme)                | Security rules for eslint                                                                                                                       | 1.4.0       | dev        |
| [eslint-plugin-standard@3.0.1](https://github.com/xjamundx/eslint-plugin-standard#readme)                    | ESlint Plugin for the Standard Linter                                                                                                           | 3.0.1       | dev        |
| [eslint-plugin-xss@0.1.8](https://npmjs.org/package/eslint-plugin-xss)                                       | Validates XSS related issues of mixing HTML and non-HTML content in variables.                                                                  | 0.1.8       | dev        |
| [fixpack@2.3.1](https://github.com/henrikjoreteg/fixpack)                                                    | cli tool that cleans up package.json files.                                                                                                     | 2.3.1       | dev        |
| [jest@21.1.0](http://facebook.github.io/jest/)                                                               | Delightful JavaScript Testing.                                                                                                                  | 21.1.0      | dev        |
| [jest-cli@21.1.0](http://facebook.github.io/jest/)                                                           | Delightful JavaScript Testing.                                                                                                                  | 21.1.0      | dev        |
| [markdown-toc@1.2.0](https://github.com/jonschlinkert/markdown-toc)                                          | Generate a markdown TOC (table of contents) with Remarkable.                                                                                    | 1.2.0       | dev        |
| [nsp@2.8.0](https://github.com/nodesecurity/nsp#readme)                                                      | The Node Security (nodesecurity.io) command line interface                                                                                      | 2.8.0       | dev        |
| [standard-version@4.2.0](https://github.com/conventional-changelog/standard-version#readme)                  | replacement for `npm version` with automatic CHANGELOG generation                                                                               | 4.2.0       | dev        |
| [yeoman-assert@3.1.0](http://yeoman.io)                                                                      | Assert utility from yeoman                                                                                                                      | 3.1.0       | dev        |
| [yeoman-test@1.7.0](http://yeoman.io/authoring/testing.html)                                                 | Test utilities for Yeoman generators                                                                                                            | 1.7.0       | dev        |

## 8. **APIs**

### 8.1. Command-line interfaces

Read our [guidelines for Command line interfaces][wiki-cli-apis].

### 8.2. Web services

Read our [guidelines for Web services][wiki-web-service-apis].

## 9. **Licensing**

Make sure you use resources that you have the rights to use. If you use libraries, remember to look for MIT, Apache or BSD but if you modify them, then take a look into license details. Copyrighted images and videos may cause legal problems.

[wiki-cli-apis]: https://gitlab.com/zaira-ardor-llc/products/home-depot-product-extractor/-/wikis/Development-guidelines/2.-Command-line-interfaces
[wiki-web-service-apis]: https://gitlab.com/zaira-ardor-llc/products/home-depot-product-extractor/-/wikis/Development-guidelines/3.-Web-services

[gl-icon-abuse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/abuse.svg?ref_type=heads
[gl-icon-accessibility]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/accessibility.svg?ref_type=heads
[gl-icon-account]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/account.svg?ref_type=heads
[gl-icon-admin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/admin.svg?ref_type=heads
[gl-icon-api]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads
[gl-icon-appearance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/appearance.svg?ref_type=heads
[gl-icon-applications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/applications.svg?ref_type=heads
[gl-icon-approval-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval-solid.svg?ref_type=heads
[gl-icon-approval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval.svg?ref_type=heads
[gl-icon-archive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/archive.svg?ref_type=heads
[gl-icon-arrow-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-down.svg?ref_type=heads
[gl-icon-arrow-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-left.svg?ref_type=heads
[gl-icon-arrow-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-right.svg?ref_type=heads
[gl-icon-arrow-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-up.svg?ref_type=heads
[gl-icon-assignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/assignee.svg?ref_type=heads
[gl-icon-at]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/at.svg?ref_type=heads
[gl-icon-attention-solid-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid-sm.svg?ref_type=heads
[gl-icon-attention-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid.svg?ref_type=heads
[gl-icon-attention]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention.svg?ref_type=heads
[gl-icon-autoplay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/autoplay.svg?ref_type=heads
[gl-icon-bitbucket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bitbucket.svg?ref_type=heads
[gl-icon-bold]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bold.svg?ref_type=heads
[gl-icon-book]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/book.svg?ref_type=heads
[gl-icon-bookmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bookmark.svg?ref_type=heads
[gl-icon-branch-deleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch-deleted.svg?ref_type=heads
[gl-icon-branch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch.svg?ref_type=heads
[gl-icon-brand-zoom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/brand-zoom.svg?ref_type=heads
[gl-icon-bug]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bug.svg?ref_type=heads
[gl-icon-building]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/building.svg?ref_type=heads
[gl-icon-bulb]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bulb.svg?ref_type=heads
[gl-icon-bullhorn]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bullhorn.svg?ref_type=heads
[gl-icon-calendar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/calendar.svg?ref_type=heads
[gl-icon-cancel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cancel.svg?ref_type=heads
[gl-icon-canceled-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/canceled-circle.svg?ref_type=heads
[gl-icon-car]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/car.svg?ref_type=heads
[gl-icon-catalog-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/catalog-checkmark.svg?ref_type=heads
[gl-icon-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chart.svg?ref_type=heads
[gl-icon-check-circle-dashed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-dashed.svg?ref_type=heads
[gl-icon-check-circle-filled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-filled.svg?ref_type=heads
[gl-icon-check-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle.svg?ref_type=heads
[gl-icon-check-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-sm.svg?ref_type=heads
[gl-icon-check-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-xs.svg?ref_type=heads
[gl-icon-check]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check.svg?ref_type=heads
[gl-icon-cherry-pick-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cherry-pick-commit.svg?ref_type=heads
[gl-icon-chevron-double-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-left.svg?ref_type=heads
[gl-icon-chevron-double-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-right.svg?ref_type=heads
[gl-icon-chevron-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-down.svg?ref_type=heads
[gl-icon-chevron-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-left.svg?ref_type=heads
[gl-icon-chevron-lg-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-down.svg?ref_type=heads
[gl-icon-chevron-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-left.svg?ref_type=heads
[gl-icon-chevron-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-right.svg?ref_type=heads
[gl-icon-chevron-lg-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-up.svg?ref_type=heads
[gl-icon-chevron-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-right.svg?ref_type=heads
[gl-icon-chevron-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-up.svg?ref_type=heads
[gl-icon-clear-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear-all.svg?ref_type=heads
[gl-icon-clear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear.svg?ref_type=heads
[gl-icon-clock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clock.svg?ref_type=heads
[gl-icon-close-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close-xs.svg?ref_type=heads
[gl-icon-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close.svg?ref_type=heads
[gl-icon-cloud-gear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-gear.svg?ref_type=heads
[gl-icon-cloud-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-pod.svg?ref_type=heads
[gl-icon-cloud-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-terminal.svg?ref_type=heads
[gl-icon-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/code.svg?ref_type=heads
[gl-icon-collapse-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-left.svg?ref_type=heads
[gl-icon-collapse-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-right.svg?ref_type=heads
[gl-icon-collapse-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-solid.svg?ref_type=heads
[gl-icon-collapse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse.svg?ref_type=heads
[gl-icon-comment-dots]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-dots.svg?ref_type=heads
[gl-icon-comment-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-lines.svg?ref_type=heads
[gl-icon-comment-next]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-next.svg?ref_type=heads
[gl-icon-comment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment.svg?ref_type=heads
[gl-icon-comments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comments.svg?ref_type=heads
[gl-icon-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg?ref_type=heads
[gl-icon-comparison]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comparison.svg?ref_type=heads
[gl-icon-compass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/compass.svg?ref_type=heads
[gl-icon-connected]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/connected.svg?ref_type=heads
[gl-icon-container-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/container-image.svg?ref_type=heads
[gl-icon-copy-to-clipboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/copy-to-clipboard.svg?ref_type=heads
[gl-icon-credit-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/credit-card.svg?ref_type=heads
[gl-icon-dash-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash-circle.svg?ref_type=heads
[gl-icon-dash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash.svg?ref_type=heads
[gl-icon-dashboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dashboard.svg?ref_type=heads
[gl-icon-deployments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/deployments.svg?ref_type=heads
[gl-icon-details-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/details-block.svg?ref_type=heads
[gl-icon-diagram]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads
[gl-icon-discord]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/discord.svg?ref_type=heads
[gl-icon-disk]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/disk.svg?ref_type=heads
[gl-icon-doc-changes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-changes.svg?ref_type=heads
[gl-icon-doc-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-chart.svg?ref_type=heads
[gl-icon-doc-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-code.svg?ref_type=heads
[gl-icon-doc-compressed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-compressed.svg?ref_type=heads
[gl-icon-doc-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-expand.svg?ref_type=heads
[gl-icon-doc-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-image.svg?ref_type=heads
[gl-icon-doc-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-new.svg?ref_type=heads
[gl-icon-doc-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-symlink.svg?ref_type=heads
[gl-icon-doc-text]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-text.svg?ref_type=heads
[gl-icon-doc-versions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-versions.svg?ref_type=heads
[gl-icon-document]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/document.svg?ref_type=heads
[gl-icon-documents]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/documents.svg?ref_type=heads
[gl-icon-dot-grid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dot-grid.svg?ref_type=heads
[gl-icon-dotted-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dotted-circle.svg?ref_type=heads
[gl-icon-double-headed-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/double-headed-arrow.svg?ref_type=heads
[gl-icon-download]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/download.svg?ref_type=heads
[gl-icon-dumbbell]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dumbbell.svg?ref_type=heads
[gl-icon-duplicate]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/duplicate.svg?ref_type=heads
[gl-icon-earth]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/earth.svg?ref_type=heads
[gl-icon-ellipsis_h]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_h.svg?ref_type=heads
[gl-icon-ellipsis_v]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_v.svg?ref_type=heads
[gl-icon-entity-blocked]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/entity-blocked.svg?ref_type=heads
[gl-icon-environment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/environment.svg?ref_type=heads
[gl-icon-epic-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic-closed.svg?ref_type=heads
[gl-icon-epic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic.svg?ref_type=heads
[gl-icon-error]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/error.svg?ref_type=heads
[gl-icon-expand-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-down.svg?ref_type=heads
[gl-icon-expand-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-left.svg?ref_type=heads
[gl-icon-expand-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-right.svg?ref_type=heads
[gl-icon-expand-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-up.svg?ref_type=heads
[gl-icon-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand.svg?ref_type=heads
[gl-icon-expire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expire.svg?ref_type=heads
[gl-icon-export]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/export.svg?ref_type=heads
[gl-icon-external-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/external-link.svg?ref_type=heads
[gl-icon-eye-slash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye-slash.svg?ref_type=heads
[gl-icon-eye]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye.svg?ref_type=heads
[gl-icon-face-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-neutral.svg?ref_type=heads
[gl-icon-face-unhappy]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-unhappy.svg?ref_type=heads
[gl-icon-false-positive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/false-positive.svg?ref_type=heads
[gl-icon-feature-flag-disabled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag-disabled.svg?ref_type=heads
[gl-icon-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag.svg?ref_type=heads
[gl-icon-file-addition-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition-solid.svg?ref_type=heads
[gl-icon-file-addition]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition.svg?ref_type=heads
[gl-icon-file-deletion-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion-solid.svg?ref_type=heads
[gl-icon-file-deletion]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion.svg?ref_type=heads
[gl-icon-file-modified-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified-solid.svg?ref_type=heads
[gl-icon-file-modified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified.svg?ref_type=heads
[gl-icon-file-tree]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-tree.svg?ref_type=heads
[gl-icon-filter]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/filter.svg?ref_type=heads
[gl-icon-fire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fire.svg?ref_type=heads
[gl-icon-first-contribution]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/first-contribution.svg?ref_type=heads
[gl-icon-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/flag.svg?ref_type=heads
[gl-icon-folder-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-new.svg?ref_type=heads
[gl-icon-folder-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-o.svg?ref_type=heads
[gl-icon-folder-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-open.svg?ref_type=heads
[gl-icon-folder]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder.svg?ref_type=heads
[gl-icon-food]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/food.svg?ref_type=heads
[gl-icon-fork]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fork.svg?ref_type=heads
[gl-icon-formula]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/formula.svg?ref_type=heads
[gl-icon-git-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git-merge.svg?ref_type=heads
[gl-icon-git]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git.svg?ref_type=heads
[gl-icon-gitea]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/gitea.svg?ref_type=heads
[gl-icon-github]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/github.svg?ref_type=heads
[gl-icon-go-back]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/go-back.svg?ref_type=heads
[gl-icon-google]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/google.svg?ref_type=heads
[gl-icon-grip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/grip.svg?ref_type=heads
[gl-icon-group]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/group.svg?ref_type=heads
[gl-icon-hamburger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hamburger.svg?ref_type=heads
[gl-icon-heading]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heading.svg?ref_type=heads
[gl-icon-heart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heart.svg?ref_type=heads
[gl-icon-highlight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/highlight.svg?ref_type=heads
[gl-icon-history]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/history.svg?ref_type=heads
[gl-icon-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/home.svg?ref_type=heads
[gl-icon-hook]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hook.svg?ref_type=heads
[gl-icon-hourglass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hourglass.svg?ref_type=heads
[gl-icon-image-comment-dark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-dark.svg?ref_type=heads
[gl-icon-image-comment-light]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-light.svg?ref_type=heads
[gl-icon-import]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/import.svg?ref_type=heads
[gl-icon-incognito]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/incognito.svg?ref_type=heads
[gl-icon-information-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information-o.svg?ref_type=heads
[gl-icon-information]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information.svg?ref_type=heads
[gl-icon-infrastructure-registry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/infrastructure-registry.svg?ref_type=heads
[gl-icon-issue-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-block.svg?ref_type=heads
[gl-icon-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-close.svg?ref_type=heads
[gl-icon-issue-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-closed.svg?ref_type=heads
[gl-icon-issue-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-new.svg?ref_type=heads
[gl-icon-issue-open-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-open-m.svg?ref_type=heads
[gl-icon-issue-type-enhancement]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-enhancement.svg?ref_type=heads
[gl-icon-issue-type-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature-flag.svg?ref_type=heads
[gl-icon-issue-type-feature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature.svg?ref_type=heads
[gl-icon-issue-type-incident]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-incident.svg?ref_type=heads
[gl-icon-issue-type-issue]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-issue.svg?ref_type=heads
[gl-icon-issue-type-keyresult]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-keyresult.svg?ref_type=heads
[gl-icon-issue-type-maintenance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-maintenance.svg?ref_type=heads
[gl-icon-issue-type-objective]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-objective.svg?ref_type=heads
[gl-icon-issue-type-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-requirements.svg?ref_type=heads
[gl-icon-issue-type-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-task.svg?ref_type=heads
[gl-icon-issue-type-test-case]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-test-case.svg?ref_type=heads
[gl-icon-issue-type-ticket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-ticket.svg?ref_type=heads
[gl-icon-issue-update]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-update.svg?ref_type=heads
[gl-icon-issues]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issues.svg?ref_type=heads
[gl-icon-italic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/italic.svg?ref_type=heads
[gl-icon-iteration]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/iteration.svg?ref_type=heads
[gl-icon-key]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/key.svg?ref_type=heads
[gl-icon-keyboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/keyboard.svg?ref_type=heads
[gl-icon-kubernetes-agent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes-agent.svg?ref_type=heads
[gl-icon-kubernetes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes.svg?ref_type=heads
[gl-icon-label]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/label.svg?ref_type=heads
[gl-icon-labels]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/labels.svg?ref_type=heads
[gl-icon-leave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/leave.svg?ref_type=heads
[gl-icon-level-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/level-up.svg?ref_type=heads
[gl-icon-license-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license-sm.svg?ref_type=heads
[gl-icon-license]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license.svg?ref_type=heads
[gl-icon-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/link.svg?ref_type=heads
[gl-icon-linkedin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/linkedin.svg?ref_type=heads
[gl-icon-list-bulleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-bulleted.svg?ref_type=heads
[gl-icon-list-indent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-indent.svg?ref_type=heads
[gl-icon-list-numbered]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-numbered.svg?ref_type=heads
[gl-icon-list-outdent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-outdent.svg?ref_type=heads
[gl-icon-list-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-task.svg?ref_type=heads
[gl-icon-live-preview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-preview.svg?ref_type=heads
[gl-icon-live-stream]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-stream.svg?ref_type=heads
[gl-icon-location-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location-dot.svg?ref_type=heads
[gl-icon-location]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location.svg?ref_type=heads
[gl-icon-lock-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock-open.svg?ref_type=heads
[gl-icon-lock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock.svg?ref_type=heads
[gl-icon-log]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/log.svg?ref_type=heads
[gl-icon-long-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/long-arrow.svg?ref_type=heads
[gl-icon-machine-learning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/machine-learning.svg?ref_type=heads
[gl-icon-mail]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mail.svg?ref_type=heads
[gl-icon-markdown-mark-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark-solid.svg?ref_type=heads
[gl-icon-markdown-mark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark.svg?ref_type=heads
[gl-icon-marquee-selection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/marquee-selection.svg?ref_type=heads
[gl-icon-mastodon]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mastodon.svg?ref_type=heads
[gl-icon-maximize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/maximize.svg?ref_type=heads
[gl-icon-media-broken]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media-broken.svg?ref_type=heads
[gl-icon-media]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media.svg?ref_type=heads
[gl-icon-merge-request-close-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close-m.svg?ref_type=heads
[gl-icon-merge-request-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close.svg?ref_type=heads
[gl-icon-merge-request-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-open.svg?ref_type=heads
[gl-icon-merge-request]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request.svg?ref_type=heads
[gl-icon-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge.svg?ref_type=heads
[gl-icon-messages]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/messages.svg?ref_type=heads
[gl-icon-milestone]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/milestone.svg?ref_type=heads
[gl-icon-minimize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/minimize.svg?ref_type=heads
[gl-icon-mobile-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile-issue-close.svg?ref_type=heads
[gl-icon-mobile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile.svg?ref_type=heads
[gl-icon-monitor-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-lines.svg?ref_type=heads
[gl-icon-monitor-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-o.svg?ref_type=heads
[gl-icon-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor.svg?ref_type=heads
[gl-icon-namespace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/namespace.svg?ref_type=heads
[gl-icon-nature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/nature.svg?ref_type=heads
[gl-icon-notifications-off]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications-off.svg?ref_type=heads
[gl-icon-notifications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications.svg?ref_type=heads
[gl-icon-object]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/object.svg?ref_type=heads
[gl-icon-organization]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/organization.svg?ref_type=heads
[gl-icon-overview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/overview.svg?ref_type=heads
[gl-icon-package]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/package.svg?ref_type=heads
[gl-icon-paper-airplane]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paper-airplane.svg?ref_type=heads
[gl-icon-paperclip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paperclip.svg?ref_type=heads
[gl-icon-partner-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/partner-verified.svg?ref_type=heads
[gl-icon-pause]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pause.svg?ref_type=heads
[gl-icon-pencil-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil-square.svg?ref_type=heads
[gl-icon-pencil]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil.svg?ref_type=heads
[gl-icon-pipeline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pipeline.svg?ref_type=heads
[gl-icon-planning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/planning.svg?ref_type=heads
[gl-icon-play]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/play.svg?ref_type=heads
[gl-icon-plus-square-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square-o.svg?ref_type=heads
[gl-icon-plus-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square.svg?ref_type=heads
[gl-icon-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus.svg?ref_type=heads
[gl-icon-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pod.svg?ref_type=heads
[gl-icon-podcast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/podcast.svg?ref_type=heads
[gl-icon-power]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/power.svg?ref_type=heads
[gl-icon-preferences]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/preferences.svg?ref_type=heads
[gl-icon-profile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/profile.svg?ref_type=heads
[gl-icon-progress]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/progress.svg?ref_type=heads
[gl-icon-project]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/project.svg?ref_type=heads
[gl-icon-push-rules]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/push-rules.svg?ref_type=heads
[gl-icon-question-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question-o.svg?ref_type=heads
[gl-icon-question]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question.svg?ref_type=heads
[gl-icon-quick-actions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quick-actions.svg?ref_type=heads
[gl-icon-quota]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quota.svg?ref_type=heads
[gl-icon-quote]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quote.svg?ref_type=heads
[gl-icon-redo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/redo.svg?ref_type=heads
[gl-icon-regular-expression]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/regular-expression.svg?ref_type=heads
[gl-icon-remove-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove-all.svg?ref_type=heads
[gl-icon-remove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove.svg?ref_type=heads
[gl-icon-repeat]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/repeat.svg?ref_type=heads
[gl-icon-reply]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/reply.svg?ref_type=heads
[gl-icon-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/requirements.svg?ref_type=heads
[gl-icon-retry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/retry.svg?ref_type=heads
[gl-icon-review-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-checkmark.svg?ref_type=heads
[gl-icon-review-list]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-list.svg?ref_type=heads
[gl-icon-review-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-warning.svg?ref_type=heads
[gl-icon-rocket-launch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket-launch.svg?ref_type=heads
[gl-icon-rocket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket.svg?ref_type=heads
[gl-icon-rss]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rss.svg?ref_type=heads
[gl-icon-scale]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scale.svg?ref_type=heads
[gl-icon-scroll-handle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll-handle.svg?ref_type=heads
[gl-icon-scroll_down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_down.svg?ref_type=heads
[gl-icon-scroll_up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_up.svg?ref_type=heads
[gl-icon-search-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-dot.svg?ref_type=heads
[gl-icon-search-minus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-minus.svg?ref_type=heads
[gl-icon-search-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-plus.svg?ref_type=heads
[gl-icon-search-results]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-results.svg?ref_type=heads
[gl-icon-search-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-sm.svg?ref_type=heads
[gl-icon-search]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search.svg?ref_type=heads
[gl-icon-settings]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/settings.svg?ref_type=heads
[gl-icon-severity-critical]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-critical.svg?ref_type=heads
[gl-icon-severity-high]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-high.svg?ref_type=heads
[gl-icon-severity-info]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-info.svg?ref_type=heads
[gl-icon-severity-low]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-low.svg?ref_type=heads
[gl-icon-severity-medium]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-medium.svg?ref_type=heads
[gl-icon-severity-unknown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-unknown.svg?ref_type=heads
[gl-icon-share]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/share.svg?ref_type=heads
[gl-icon-shield]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/shield.svg?ref_type=heads
[gl-icon-sidebar-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar-right.svg?ref_type=heads
[gl-icon-sidebar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar.svg?ref_type=heads
[gl-icon-skype]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/skype.svg?ref_type=heads
[gl-icon-slight-frown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-frown.svg?ref_type=heads
[gl-icon-slight-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-smile.svg?ref_type=heads
[gl-icon-smart-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smart-card.svg?ref_type=heads
[gl-icon-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smile.svg?ref_type=heads
[gl-icon-smiley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smiley.svg?ref_type=heads
[gl-icon-snippet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/snippet.svg?ref_type=heads
[gl-icon-soft-unwrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-unwrap.svg?ref_type=heads
[gl-icon-soft-wrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-wrap.svg?ref_type=heads
[gl-icon-sort-highest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-highest.svg?ref_type=heads
[gl-icon-sort-lowest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-lowest.svg?ref_type=heads
[gl-icon-spam]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spam.svg?ref_type=heads
[gl-icon-spinner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spinner.svg?ref_type=heads
[gl-icon-stage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stage-all.svg?ref_type=heads
[gl-icon-star-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star-o.svg?ref_type=heads
[gl-icon-star]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star.svg?ref_type=heads
[gl-icon-status-active]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-active.svg?ref_type=heads
[gl-icon-status-alert]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-alert.svg?ref_type=heads
[gl-icon-status-cancelled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-cancelled.svg?ref_type=heads
[gl-icon-status-failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-failed.svg?ref_type=heads
[gl-icon-status-health]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-health.svg?ref_type=heads
[gl-icon-status-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-neutral.svg?ref_type=heads
[gl-icon-status-paused]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-paused.svg?ref_type=heads
[gl-icon-status-running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-running.svg?ref_type=heads
[gl-icon-status-scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-scheduled.svg?ref_type=heads
[gl-icon-status-stopped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-stopped.svg?ref_type=heads
[gl-icon-status-success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-success.svg?ref_type=heads
[gl-icon-status-waiting]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-waiting.svg?ref_type=heads
[gl-icon-status]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status.svg?ref_type=heads
[gl-icon-status_canceled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled.svg?ref_type=heads
[gl-icon-status_canceled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled_borderless.svg?ref_type=heads
[gl-icon-status_closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_closed.svg?ref_type=heads
[gl-icon-status_created]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created.svg?ref_type=heads
[gl-icon-status_created_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created_borderless.svg?ref_type=heads
[gl-icon-status_failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed.svg?ref_type=heads
[gl-icon-status_failed_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed_borderless.svg?ref_type=heads
[gl-icon-status_manual]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual.svg?ref_type=heads
[gl-icon-status_manual_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual_borderless.svg?ref_type=heads
[gl-icon-status_notfound]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound.svg?ref_type=heads
[gl-icon-status_notfound_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound_borderless.svg?ref_type=heads
[gl-icon-status_open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_open.svg?ref_type=heads
[gl-icon-status_pending]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending.svg?ref_type=heads
[gl-icon-status_pending_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending_borderless.svg?ref_type=heads
[gl-icon-status_preparing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing.svg?ref_type=heads
[gl-icon-status_preparing_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing_borderless.svg?ref_type=heads
[gl-icon-status_running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running.svg?ref_type=heads
[gl-icon-status_running_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running_borderless.svg?ref_type=heads
[gl-icon-status_scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled.svg?ref_type=heads
[gl-icon-status_scheduled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled_borderless.svg?ref_type=heads
[gl-icon-status_skipped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped.svg?ref_type=heads
[gl-icon-status_skipped_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped_borderless.svg?ref_type=heads
[gl-icon-status_success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success.svg?ref_type=heads
[gl-icon-status_success_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_borderless.svg?ref_type=heads
[gl-icon-status_success_solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_solid.svg?ref_type=heads
[gl-icon-status_warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning.svg?ref_type=heads
[gl-icon-status_warning_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning_borderless.svg?ref_type=heads
[gl-icon-stop]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stop.svg?ref_type=heads
[gl-icon-strikethrough]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/strikethrough.svg?ref_type=heads
[gl-icon-subgroup]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subgroup.svg?ref_type=heads
[gl-icon-subscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subscript.svg?ref_type=heads
[gl-icon-substitute]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/substitute.svg?ref_type=heads
[gl-icon-superscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/superscript.svg?ref_type=heads
[gl-icon-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/symlink.svg?ref_type=heads
[gl-icon-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/table.svg?ref_type=heads
[gl-icon-tablet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tablet.svg?ref_type=heads
[gl-icon-tachometer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tachometer.svg?ref_type=heads
[gl-icon-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tag.svg?ref_type=heads
[gl-icon-tanuki-ai]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-ai.svg?ref_type=heads
[gl-icon-tanuki-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-verified.svg?ref_type=heads
[gl-icon-tanuki]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki.svg?ref_type=heads
[gl-icon-task-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/task-done.svg?ref_type=heads
[gl-icon-template]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/template.svg?ref_type=heads
[gl-icon-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terminal.svg?ref_type=heads
[gl-icon-terraform]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terraform.svg?ref_type=heads
[gl-icon-text-description]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/text-description.svg?ref_type=heads
[gl-icon-thumb-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-down.svg?ref_type=heads
[gl-icon-thumb-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-up.svg?ref_type=heads
[gl-icon-thumbtack-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack-solid.svg?ref_type=heads
[gl-icon-thumbtack]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack.svg?ref_type=heads
[gl-icon-time-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/time-out.svg?ref_type=heads
[gl-icon-timer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/timer.svg?ref_type=heads
[gl-icon-title]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/title.svg?ref_type=heads
[gl-icon-todo-add]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-add.svg?ref_type=heads
[gl-icon-todo-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-done.svg?ref_type=heads
[gl-icon-token]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/token.svg?ref_type=heads
[gl-icon-trend-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-down.svg?ref_type=heads
[gl-icon-trend-static]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-static.svg?ref_type=heads
[gl-icon-trend-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-up.svg?ref_type=heads
[gl-icon-trigger-source]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trigger-source.svg?ref_type=heads
[gl-icon-unapproval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unapproval.svg?ref_type=heads
[gl-icon-unassignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unassignee.svg?ref_type=heads
[gl-icon-underline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/underline.svg?ref_type=heads
[gl-icon-unlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unlink.svg?ref_type=heads
[gl-icon-unstage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unstage-all.svg?ref_type=heads
[gl-icon-upgrade]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upgrade.svg?ref_type=heads
[gl-icon-upload]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upload.svg?ref_type=heads
[gl-icon-user]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/user.svg?ref_type=heads
[gl-icon-users]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/users.svg?ref_type=heads
[gl-icon-volume-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/volume-up.svg?ref_type=heads
[gl-icon-warning-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning-solid.svg?ref_type=heads
[gl-icon-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning.svg?ref_type=heads
[gl-icon-weight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/weight.svg?ref_type=heads
[gl-icon-work]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/work.svg?ref_type=heads
[gl-icon-x]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/x.svg?ref_type=heads

