# <img align="bottom" alt="bug" src="https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bug.svg" height="30" width="30"> Defect incident report

## 1. Summary of expected behavior

> ⌦ Describe what you _wanted_ to accomplish, in what role/capacity, and why
> it's important to you.

As a {role}, <br>
I must/need/want/should {do something} <br>
in order to {achieve value}.

## 2. Acceptance criteria

> ⌦ Replace the examples below with your own imperative, "true/false"
> statements` for the **behavior you expect** to see, or the behavior that
> **would** be true if there were no errors.

_When working properly, I expect that:_

1. [ ] Job Applicants receive a confirmation email after they submit their
       resumes.
1. [ ] An Applicant's resume information isn't lost when errors occur.
1. [ ] {criterion-three}
1. [ ] {criterion-four}

## 3. Steps to reproduce

>>>
⌦ Provide a link to a live example, or

⌦ Replace the examples below with an unambiguous sequence of instructions that end with proof of the defect.

⌦ Include source code and log files, if relevant and available.
>>>

1.  Enter the words "...." into the "Search" text field.
2.  Select the Search button.
3.  Next...
4.  Then...
5.  Finally...
6.  _X_ does not work as expected.

## 4. Your environment

> ⌦ Include as many relevant details about the environment in which the defect
> occured.

> ⌦ Include as many relevant details about the environment in which the defect
> occured.

- **cost-estimating** version:
- `JavaScript` version (`node --version`):
- `npm` version (`npm --version`):
- Operating System and version (desktop or mobile):

  > **💡 MacOS**
  >
  > - 1.  Open a Terminal
  > - 2.  Run
  >       `system_profiler SPHardwareDataType SPSoftwareDataType -detailLevel mini | pbcopy`
  > - 3.  Paste (<kbd>command ⌘</kbd> + <kbd>v</kbd>) below:

<!-- ⛔️ Octicon img references ⛔️  -->

[gl-icon-abuse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/abuse.svg
[gl-icon-accessibility]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/accessibility.svg
[gl-icon-account]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/account.svg
[gl-icon-admin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/admin.svg
[gl-icon-api]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg
[gl-icon-appearance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/appearance.svg
[gl-icon-applications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/applications.svg
[gl-icon-approval-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval-solid.svg
[gl-icon-approval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval.svg
[gl-icon-archive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/archive.svg
[gl-icon-arrow-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-down.svg
[gl-icon-arrow-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-left.svg
[gl-icon-arrow-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-right.svg
[gl-icon-arrow-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-up.svg
[gl-icon-assignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/assignee.svg
[gl-icon-at]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/at.svg
[gl-icon-attention-solid-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid-sm.svg
[gl-icon-attention-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid.svg
[gl-icon-attention]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention.svg
[gl-icon-autoplay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/autoplay.svg
[gl-icon-bitbucket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bitbucket.svg
[gl-icon-bold]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bold.svg
[gl-icon-book]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/book.svg
[gl-icon-bookmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bookmark.svg
[gl-icon-branch-deleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch-deleted.svg
[gl-icon-branch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch.svg
[gl-icon-brand-zoom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/brand-zoom.svg
[gl-icon-bug]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bug.svg
[gl-icon-building]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/building.svg
[gl-icon-bulb]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bulb.svg
[gl-icon-bullhorn]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bullhorn.svg
[gl-icon-calendar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/calendar.svg
[gl-icon-cancel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cancel.svg
[gl-icon-canceled-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/canceled-circle.svg
[gl-icon-car]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/car.svg
[gl-icon-catalog-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/catalog-checkmark.svg
[gl-icon-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chart.svg
[gl-icon-check-circle-dashed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-dashed.svg
[gl-icon-check-circle-filled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-filled.svg
[gl-icon-check-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle.svg
[gl-icon-check-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-sm.svg
[gl-icon-check-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-xs.svg
[gl-icon-check]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check.svg
[gl-icon-cherry-pick-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cherry-pick-commit.svg
[gl-icon-chevron-double-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-left.svg
[gl-icon-chevron-double-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-right.svg
[gl-icon-chevron-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-down.svg
[gl-icon-chevron-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-left.svg
[gl-icon-chevron-lg-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-down.svg
[gl-icon-chevron-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-left.svg
[gl-icon-chevron-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-right.svg
[gl-icon-chevron-lg-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-up.svg
[gl-icon-chevron-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-right.svg
[gl-icon-chevron-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-up.svg
[gl-icon-clear-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear-all.svg
[gl-icon-clear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear.svg
[gl-icon-clock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clock.svg
[gl-icon-close-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close-xs.svg
[gl-icon-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close.svg
[gl-icon-cloud-gear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-gear.svg
[gl-icon-cloud-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-pod.svg
[gl-icon-cloud-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-terminal.svg
[gl-icon-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/code.svg
[gl-icon-collapse-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-left.svg
[gl-icon-collapse-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-right.svg
[gl-icon-collapse-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-solid.svg
[gl-icon-collapse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse.svg
[gl-icon-comment-dots]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-dots.svg
[gl-icon-comment-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-lines.svg
[gl-icon-comment-next]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-next.svg
[gl-icon-comment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment.svg
[gl-icon-comments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comments.svg
[gl-icon-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg
[gl-icon-comparison]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comparison.svg
[gl-icon-compass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/compass.svg
[gl-icon-connected]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/connected.svg
[gl-icon-container-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/container-image.svg
[gl-icon-copy-to-clipboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/copy-to-clipboard.svg
[gl-icon-credit-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/credit-card.svg
[gl-icon-dash-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash-circle.svg
[gl-icon-dash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash.svg
[gl-icon-dashboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dashboard.svg
[gl-icon-deployments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/deployments.svg
[gl-icon-details-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/details-block.svg
[gl-icon-diagram]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg
[gl-icon-discord]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/discord.svg
[gl-icon-disk]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/disk.svg
[gl-icon-doc-changes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-changes.svg
[gl-icon-doc-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-chart.svg
[gl-icon-doc-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-code.svg
[gl-icon-doc-compressed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-compressed.svg
[gl-icon-doc-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-expand.svg
[gl-icon-doc-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-image.svg
[gl-icon-doc-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-new.svg
[gl-icon-doc-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-symlink.svg
[gl-icon-doc-text]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-text.svg
[gl-icon-doc-versions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-versions.svg
[gl-icon-document]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/document.svg
[gl-icon-documents]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/documents.svg
[gl-icon-dot-grid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dot-grid.svg
[gl-icon-dotted-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dotted-circle.svg
[gl-icon-double-headed-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/double-headed-arrow.svg
[gl-icon-download]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/download.svg
[gl-icon-dumbbell]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dumbbell.svg
[gl-icon-duplicate]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/duplicate.svg
[gl-icon-earth]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/earth.svg
[gl-icon-ellipsis_h]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_h.svg
[gl-icon-ellipsis_v]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_v.svg
[gl-icon-entity-blocked]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/entity-blocked.svg
[gl-icon-environment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/environment.svg
[gl-icon-epic-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic-closed.svg
[gl-icon-epic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic.svg
[gl-icon-error]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/error.svg
[gl-icon-expand-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-down.svg
[gl-icon-expand-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-left.svg
[gl-icon-expand-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-right.svg
[gl-icon-expand-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-up.svg
[gl-icon-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand.svg
[gl-icon-expire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expire.svg
[gl-icon-export]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/export.svg
[gl-icon-external-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/external-link.svg
[gl-icon-eye-slash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye-slash.svg
[gl-icon-eye]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye.svg
[gl-icon-face-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-neutral.svg
[gl-icon-face-unhappy]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-unhappy.svg
[gl-icon-false-positive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/false-positive.svg
[gl-icon-feature-flag-disabled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag-disabled.svg
[gl-icon-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag.svg
[gl-icon-file-addition-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition-solid.svg
[gl-icon-file-addition]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition.svg
[gl-icon-file-deletion-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion-solid.svg
[gl-icon-file-deletion]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion.svg
[gl-icon-file-modified-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified-solid.svg
[gl-icon-file-modified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified.svg
[gl-icon-file-tree]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-tree.svg
[gl-icon-filter]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/filter.svg
[gl-icon-fire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fire.svg
[gl-icon-first-contribution]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/first-contribution.svg
[gl-icon-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/flag.svg
[gl-icon-folder-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-new.svg
[gl-icon-folder-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-o.svg
[gl-icon-folder-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-open.svg
[gl-icon-folder]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder.svg
[gl-icon-food]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/food.svg
[gl-icon-fork]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fork.svg
[gl-icon-formula]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/formula.svg
[gl-icon-git-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git-merge.svg
[gl-icon-git]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git.svg
[gl-icon-gitea]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/gitea.svg
[gl-icon-github]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/github.svg
[gl-icon-go-back]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/go-back.svg
[gl-icon-google]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/google.svg
[gl-icon-grip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/grip.svg
[gl-icon-group]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/group.svg
[gl-icon-hamburger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hamburger.svg
[gl-icon-heading]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heading.svg
[gl-icon-heart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heart.svg
[gl-icon-highlight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/highlight.svg
[gl-icon-history]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/history.svg
[gl-icon-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/home.svg
[gl-icon-hook]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hook.svg
[gl-icon-hourglass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hourglass.svg
[gl-icon-image-comment-dark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-dark.svg
[gl-icon-image-comment-light]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-light.svg
[gl-icon-import]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/import.svg
[gl-icon-incognito]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/incognito.svg
[gl-icon-information-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information-o.svg
[gl-icon-information]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information.svg
[gl-icon-infrastructure-registry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/infrastructure-registry.svg
[gl-icon-issue-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-block.svg
[gl-icon-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-close.svg
[gl-icon-issue-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-closed.svg
[gl-icon-issue-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-new.svg
[gl-icon-issue-open-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-open-m.svg
[gl-icon-issue-type-enhancement]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-enhancement.svg
[gl-icon-issue-type-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature-flag.svg
[gl-icon-issue-type-feature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature.svg
[gl-icon-issue-type-incident]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-incident.svg
[gl-icon-issue-type-issue]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-issue.svg
[gl-icon-issue-type-keyresult]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-keyresult.svg
[gl-icon-issue-type-maintenance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-maintenance.svg
[gl-icon-issue-type-objective]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-objective.svg
[gl-icon-issue-type-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-requirements.svg
[gl-icon-issue-type-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-task.svg
[gl-icon-issue-type-test-case]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-test-case.svg
[gl-icon-issue-type-ticket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-ticket.svg
[gl-icon-issue-update]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-update.svg
[gl-icon-issues]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issues.svg
[gl-icon-italic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/italic.svg
[gl-icon-iteration]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/iteration.svg
[gl-icon-key]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/key.svg
[gl-icon-keyboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/keyboard.svg
[gl-icon-kubernetes-agent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes-agent.svg
[gl-icon-kubernetes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes.svg
[gl-icon-label]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/label.svg
[gl-icon-labels]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/labels.svg
[gl-icon-leave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/leave.svg
[gl-icon-level-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/level-up.svg
[gl-icon-license-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license-sm.svg
[gl-icon-license]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license.svg
[gl-icon-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/link.svg
[gl-icon-linkedin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/linkedin.svg
[gl-icon-list-bulleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-bulleted.svg
[gl-icon-list-indent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-indent.svg
[gl-icon-list-numbered]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-numbered.svg
[gl-icon-list-outdent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-outdent.svg
[gl-icon-list-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-task.svg
[gl-icon-live-preview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-preview.svg
[gl-icon-live-stream]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-stream.svg
[gl-icon-location-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location-dot.svg
[gl-icon-location]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location.svg
[gl-icon-lock-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock-open.svg
[gl-icon-lock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock.svg
[gl-icon-log]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/log.svg
[gl-icon-long-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/long-arrow.svg
[gl-icon-machine-learning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/machine-learning.svg
[gl-icon-mail]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mail.svg
[gl-icon-markdown-mark-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark-solid.svg
[gl-icon-markdown-mark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark.svg
[gl-icon-marquee-selection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/marquee-selection.svg
[gl-icon-mastodon]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mastodon.svg
[gl-icon-maximize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/maximize.svg
[gl-icon-media-broken]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media-broken.svg
[gl-icon-media]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media.svg
[gl-icon-merge-request-close-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close-m.svg
[gl-icon-merge-request-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close.svg
[gl-icon-merge-request-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-open.svg
[gl-icon-merge-request]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request.svg
[gl-icon-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge.svg
[gl-icon-messages]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/messages.svg
[gl-icon-milestone]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/milestone.svg
[gl-icon-minimize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/minimize.svg
[gl-icon-mobile-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile-issue-close.svg
[gl-icon-mobile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile.svg
[gl-icon-monitor-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-lines.svg
[gl-icon-monitor-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-o.svg
[gl-icon-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor.svg
[gl-icon-namespace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/namespace.svg
[gl-icon-nature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/nature.svg
[gl-icon-notifications-off]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications-off.svg
[gl-icon-notifications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications.svg
[gl-icon-object]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/object.svg
[gl-icon-organization]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/organization.svg
[gl-icon-overview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/overview.svg
[gl-icon-package]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/package.svg
[gl-icon-paper-airplane]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paper-airplane.svg
[gl-icon-paperclip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paperclip.svg
[gl-icon-partner-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/partner-verified.svg
[gl-icon-pause]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pause.svg
[gl-icon-pencil-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil-square.svg
[gl-icon-pencil]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil.svg
[gl-icon-pipeline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pipeline.svg
[gl-icon-planning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/planning.svg
[gl-icon-play]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/play.svg
[gl-icon-plus-square-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square-o.svg
[gl-icon-plus-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square.svg
[gl-icon-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus.svg
[gl-icon-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pod.svg
[gl-icon-podcast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/podcast.svg
[gl-icon-power]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/power.svg
[gl-icon-preferences]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/preferences.svg
[gl-icon-profile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/profile.svg
[gl-icon-progress]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/progress.svg
[gl-icon-project]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/project.svg
[gl-icon-push-rules]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/push-rules.svg
[gl-icon-question-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question-o.svg
[gl-icon-question]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question.svg
[gl-icon-quick-actions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quick-actions.svg
[gl-icon-quota]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quota.svg
[gl-icon-quote]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quote.svg
[gl-icon-redo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/redo.svg
[gl-icon-regular-expression]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/regular-expression.svg
[gl-icon-remove-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove-all.svg
[gl-icon-remove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove.svg
[gl-icon-repeat]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/repeat.svg
[gl-icon-reply]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/reply.svg
[gl-icon-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/requirements.svg
[gl-icon-retry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/retry.svg
[gl-icon-review-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-checkmark.svg
[gl-icon-review-list]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-list.svg
[gl-icon-review-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-warning.svg
[gl-icon-rocket-launch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket-launch.svg
[gl-icon-rocket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket.svg
[gl-icon-rss]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rss.svg
[gl-icon-scale]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scale.svg
[gl-icon-scroll-handle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll-handle.svg
[gl-icon-scroll_down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_down.svg
[gl-icon-scroll_up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_up.svg
[gl-icon-search-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-dot.svg
[gl-icon-search-minus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-minus.svg
[gl-icon-search-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-plus.svg
[gl-icon-search-results]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-results.svg
[gl-icon-search-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-sm.svg
[gl-icon-search]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search.svg
[gl-icon-settings]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/settings.svg
[gl-icon-severity-critical]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-critical.svg
[gl-icon-severity-high]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-high.svg
[gl-icon-severity-info]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-info.svg
[gl-icon-severity-low]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-low.svg
[gl-icon-severity-medium]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-medium.svg
[gl-icon-severity-unknown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-unknown.svg
[gl-icon-share]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/share.svg
[gl-icon-shield]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/shield.svg
[gl-icon-sidebar-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar-right.svg
[gl-icon-sidebar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar.svg
[gl-icon-skype]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/skype.svg
[gl-icon-slight-frown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-frown.svg
[gl-icon-slight-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-smile.svg
[gl-icon-smart-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smart-card.svg
[gl-icon-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smile.svg
[gl-icon-smiley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smiley.svg
[gl-icon-snippet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/snippet.svg
[gl-icon-soft-unwrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-unwrap.svg
[gl-icon-soft-wrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-wrap.svg
[gl-icon-sort-highest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-highest.svg
[gl-icon-sort-lowest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-lowest.svg
[gl-icon-spam]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spam.svg
[gl-icon-spinner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spinner.svg
[gl-icon-stage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stage-all.svg
[gl-icon-star-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star-o.svg
[gl-icon-star]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star.svg
[gl-icon-status-active]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-active.svg
[gl-icon-status-alert]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-alert.svg
[gl-icon-status-cancelled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-cancelled.svg
[gl-icon-status-failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-failed.svg
[gl-icon-status-health]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-health.svg
[gl-icon-status-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-neutral.svg
[gl-icon-status-paused]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-paused.svg
[gl-icon-status-running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-running.svg
[gl-icon-status-scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-scheduled.svg
[gl-icon-status-stopped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-stopped.svg
[gl-icon-status-success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-success.svg
[gl-icon-status-waiting]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-waiting.svg
[gl-icon-status]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status.svg
[gl-icon-status_canceled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled.svg
[gl-icon-status_canceled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled_borderless.svg
[gl-icon-status_closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_closed.svg
[gl-icon-status_created]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created.svg
[gl-icon-status_created_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created_borderless.svg
[gl-icon-status_failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed.svg
[gl-icon-status_failed_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed_borderless.svg
[gl-icon-status_manual]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual.svg
[gl-icon-status_manual_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual_borderless.svg
[gl-icon-status_notfound]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound.svg
[gl-icon-status_notfound_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound_borderless.svg
[gl-icon-status_open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_open.svg
[gl-icon-status_pending]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending.svg
[gl-icon-status_pending_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending_borderless.svg
[gl-icon-status_preparing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing.svg
[gl-icon-status_preparing_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing_borderless.svg
[gl-icon-status_running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running.svg
[gl-icon-status_running_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running_borderless.svg
[gl-icon-status_scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled.svg
[gl-icon-status_scheduled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled_borderless.svg
[gl-icon-status_skipped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped.svg
[gl-icon-status_skipped_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped_borderless.svg
[gl-icon-status_success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success.svg
[gl-icon-status_success_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_borderless.svg
[gl-icon-status_success_solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_solid.svg
[gl-icon-status_warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning.svg
[gl-icon-status_warning_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning_borderless.svg
[gl-icon-stop]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stop.svg
[gl-icon-strikethrough]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/strikethrough.svg
[gl-icon-subgroup]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subgroup.svg
[gl-icon-subscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subscript.svg
[gl-icon-substitute]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/substitute.svg
[gl-icon-superscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/superscript.svg
[gl-icon-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/symlink.svg
[gl-icon-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/table.svg
[gl-icon-tablet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tablet.svg
[gl-icon-tachometer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tachometer.svg
[gl-icon-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tag.svg
[gl-icon-tanuki-ai]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-ai.svg
[gl-icon-tanuki-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-verified.svg
[gl-icon-tanuki]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki.svg
[gl-icon-task-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/task-done.svg
[gl-icon-template]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/template.svg
[gl-icon-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terminal.svg
[gl-icon-terraform]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terraform.svg
[gl-icon-text-description]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/text-description.svg
[gl-icon-thumb-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-down.svg
[gl-icon-thumb-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-up.svg
[gl-icon-thumbtack-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack-solid.svg
[gl-icon-thumbtack]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack.svg
[gl-icon-time-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/time-out.svg
[gl-icon-timer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/timer.svg
[gl-icon-title]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/title.svg
[gl-icon-todo-add]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-add.svg
[gl-icon-todo-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-done.svg
[gl-icon-token]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/token.svg
[gl-icon-trend-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-down.svg
[gl-icon-trend-static]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-static.svg
[gl-icon-trend-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-up.svg
[gl-icon-trigger-source]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trigger-source.svg
[gl-icon-unapproval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unapproval.svg
[gl-icon-unassignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unassignee.svg
[gl-icon-underline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/underline.svg
[gl-icon-unlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unlink.svg
[gl-icon-unstage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unstage-all.svg
[gl-icon-upgrade]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upgrade.svg
[gl-icon-upload]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upload.svg
[gl-icon-user]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/user.svg
[gl-icon-users]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/users.svg
[gl-icon-volume-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/volume-up.svg
[gl-icon-warning-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning-solid.svg
[gl-icon-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning.svg
[gl-icon-weight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/weight.svg
[gl-icon-work]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/work.svg
[gl-icon-x]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/x.svg
