# ID_NUMBER. REPLACE_THIS_PLACEHOLDER_WITH_YOUR_SUBJECT

![Decided on date][octicon-calendar] <time datetime="YYYY-MM-DD">YYYY-MM-DD</time>

## Status

| Status                           | Provision                                                                                                                                                             | Category                                                                                                                                                                                 |
| -------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![Proposed][adr-proposed-status] | _<small>Will be one of:</small>_ <br>![Adopt][adr-adopt-provision]<br>![Trial][adr-trial-provision]<br>![Assess][adr-assess-provision]<br>![Hold][adr-hold-provision] | _<small>Select one and remove the others:</small>_ <br>![Languages & frameworks][tr-lang-frameworks]<br>![Platforms][tr-platforms]<br>![Techniques][tr-techniques]<br>![Tools][tr-tools] |

## Context

> Add motivations and context…

### Stakeholders

> Identify and describe the stakeholders for the architecture decision. Stakeholders may be individuals (e.g., “Joe the CEO”), groups (e.g., “power users of our product”), organizations (e.g., “the Quality Assurance Department” or “the NSA”) as well as classes of same.
>
> Use Gitlab accounts & groups if possible, e.g., @zaira-ardor-llc or @gregswindle.

1. @stakeholder1
1. @stakeholder2
1. @stakeholder3
1. @csuite-group

### Concerns

> The stakeholders have areas of interest, called concerns, that are considered fundamental to the architecture of the system-of-interest.

1. Concern A…
2. Concern B…
3. …
4. Concern Z…

### Stakeholder-concern table

> Associate each identified concern with the identified stakeholders having that concern.
>
> Use Gitlab accounts & groups if possible, e.g., @zaira-ardor-llc > or @gregswindle.

Table 2: Stakeholder-concern traceability.

| Stakeholder  | Concern A | Concern B | …   | Concern _Z_ |
| :----------: | :-------: | :-------: | --- | :---------: |
| stakeholder1 |     -     |     x     | -   |      x      |
| stakeholder2 |     x     |     x     | -   |      x      |
| stakeholder3 |     -     |     -     | -   |      -      |
| csuite-group |     -     |     -     | x   |      -      |

## Decision

> State your decision here…

## Consequences

> State the consequences of this decision…

## References

> Remove this section if you don't need it. Otherwise, [use APA citations][citations].

<!-- ⛔️ Do not remove this line or anything under it ⛔️ -->

---

[![Need help? See the "Architecture Decision (AD) Guide"][octicon-question] _Need **help**? See the "Architecture Decision (AD) Guide."_][architecture-decisions-guide]

---

<!-- ADR workflow stage badges -->

[adr-process-proposed]: https://flat.badgen.net/badge/ADR/→+🗣+proposed/92B9C9 'Architecture decision proposal stage.'
[adr-process-vote]: https://flat.badgen.net/badge/ADR/→+🗳+vote/92B9C9 'Voting underway.'
[adr-process-resolved]: https://flat.badgen.net/badge/ADR/→+🏁+resolved/92B9C9 'Finalized architecture decisions.'

<!-- ADR status badges -->

[adr-accepted-status]: https://flat.badgen.net/badge/ADR/👍🏻accepted/242423 'An architecture decision that has been approved, with provision.'
[adr-proposed-status]: https://flat.badgen.net/badge/ADR/🗣proposed/92B9C9 'An architecture decision nominated for acceptance or rejection.'
[adr-rejected-status]: https://flat.badgen.net/badge/ADR/✖️rejected/DA2C38 'An unwanted or infeasible architecture decision.'
[adr-deprecated-status]: https://flat.badgen.net/badge/ADR/⛔️deprecated/F0B719 'This architecture decision has been withdrawn in favor of a newer alternative.'
[adr-supercedes-status]: https://flat.badgen.net/badge/ADR/➥supercedes/D49F0C 'A newer decision that replaces a deprecated ADR.'

<!-- ADR provision ("ring") badges -->

[adr-adopt-provision]: https://flat.badgen.net/badge/✓/adopt/B7B5CF "Represents something where there's no doubt that it's proven and mature for use."

[adr-trial-provision]: https://flat.badgen.net/badge/🎛/trial/B7B5CF "Ready for use, but not as completely proven as those in the \"adopt\" provision."
[adr-assess-provision]: https://flat.badgen.net/badge/💭/assess/B7B5CF "Decisions to look at closely, but not necessarily trial yet — unless you think they would be a particularly good fit for you."
[adr-hold-provision]: https://flat.badgen.net/badge/⚠️/hold/B7B5CF "For things that, even though they may be accepted in the industry, we haven't had a good experience with."

<!-- TechRadar category ("quadrant") badges -->

[tr-lang-frameworks]: https://flat.badgen.net/badge/◶/languages%20%26%20frameworks/f16179
[tr-platforms]: https://flat.badgen.net/badge/◵/platforms/cd840b
[tr-techniques]: https://flat.badgen.net/badge/◴/techniques/47a0ad
[tr-tools]: https://flat.badgen.net/badge/◷/tools/6b9e78
[architecture-decisions-guide]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Governance/Architecture-Decisions
[citations]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Style-Guides/Citations-(APA) 'Go to our Wiki page for Citation Guidelines.'
[octicon-calendar]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg
[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg
[octicon-question]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg
