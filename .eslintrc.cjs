module.exports = {
  env: {
    browser: false,
    es2022: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'standard-with-typescript'
  ],
  ignorePatterns: [
    'src/main.ts'
  ],
  settings: {
    'import/resolver': {
      'typescript': true,
      'node': true
    }
  },
  overrides: [
    {
      env: {
        node: true
      },
      files: ['.eslintrc.{js,cjs,mjs}'],
      parserOptions: {
        project: ['./tsconfig.json'],
        sourceType: 'module'
      }
    }
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint', 'import', 'n', 'promise', 'sonarjs'],
  rules: {
    '@typescript-eslint/consistent-generic-constructors': 'warn',
    'import/dynamic-import-chunkname': 'off'
  }
}
